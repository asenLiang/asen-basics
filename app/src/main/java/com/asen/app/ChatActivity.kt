package com.asen.app

import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.StickFooterChat1Binding
import com.asen.app.databinding.StickFooterChatBinding
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListActivity
import com.asen.libcommon.base.BaseListAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshLayout


/**
 * @author : asenLiang
 * @date   : 2020/12/24
 * @e-mail : liangAisiSen@163.com
 * @desc   : 普通聊天界面中键盘顶置和隐藏的处理，没有隐藏状态栏（键盘隐藏会闪）
 * 如果觉得麻烦可以集成 BaseMultiItemQuickAdapter 多布局适配器操作
 */
@Route(path = ActivityPath.HomeModule.ChatActivity)
class ChatActivity : BaseListActivity<String>() {

    /** 需要关闭侧滑退出才能不影响聊天界面的布局排版,要不然弹出键盘会顶置头部的布局 */
    //override val openBGASwipeBackHelper = false

    /** 关闭加载更多 */
    override fun openLoadMore() = false

    /** 尾部布局 */
    private lateinit var mFooter : StickFooterChatBinding

    /** item布局 */
    override fun getItemLayoutId() = R.layout.adapter_list_item

    /** 事件初始化方法 */
    override suspend fun initEvent() {
        super.initEvent()
        mFooter = StickFooterChatBinding.bind(getView(R.layout.stick_footer_chat))
        mFooter.apply {
            /** 发送文本 */
            mBtnSend.setOnClickListener { v ->
                addData(mInputText.text.toString())
                mInputText?.setText("")
                mBtnSend.visibility = View.GONE
                mBtnBoard.visibility = View.VISIBLE
                mInputText.hideKeyboard()
            }

            /** 显示选择区域 和 键盘隐藏 */
            mBtnBoard.setOnClickListener { v ->
                mInputText.hideKeyboard()
                mPanelContainer.visibility =
                    if (mPanelContainer.visibility == View.VISIBLE) View.GONE else View.VISIBLE
            }

            /**获取焦点*/
            mInputText?.setOnFocusChangeListener { _, b ->
                if (b) {
                    mPanelContainer.visibility = View.GONE
                }
            }

            /** 文本改变的时候监听 */
            mInputText.onTextChanged { s, _, _, _ ->
                mBtnSend.visibility = if (s.toString().isNotEmpty()) View.VISIBLE else View.GONE
                mBtnBoard.visibility = if (s.toString().isNotEmpty()) View.GONE else View.VISIBLE
            }
        }
    }

    /** item布局渲染 */
    override fun convert(vh: BaseViewHolder, t: String) {
        vh.setText(R.id.mInputWebsite, t ?: "")
    }

    /** item点击事件监听 */
    override fun onItemClick(a: BaseListAdapter<String>, v: View, position: Int) {
        Toast.makeText(this, "点击了 --> $position 位置 = ${a.data[position]}", Toast.LENGTH_SHORT)
            .show()
    }

    /** 监听返回按键退出监听 */
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_UP && event.keyCode == KeyEvent.KEYCODE_BACK && mFooter.mPanelContainer?.visibility == View.VISIBLE) {
            mFooter.mPanelContainer!!.visibility = View.GONE
            return super.dispatchKeyEvent(event)
        }
        return super.dispatchKeyEvent(event)
    }

    /** 上拉刷新下拉加载监听 */
    override fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {
        if (isRefresh) {
            it.finishRefresh()
            addData(0, "刷新新增")
        }
    }

}