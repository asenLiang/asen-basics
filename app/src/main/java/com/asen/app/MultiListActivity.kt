package com.asen.app

import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.StickFooterViewBinding
import com.asen.app.databinding.StickHeaderViewBinding
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListAdapter
import com.asen.libcommon.base.BaseMultiListActivity
import com.asen.libcommon.base.viewbind.viewBind
import com.scwang.smart.refresh.layout.api.RefreshLayout


/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
@Route(path = ActivityPath.HomeModule.MultiListActivity)
class MultiListActivity(
    private var mAdapter0: BaseListAdapter<String>? = null,
    private var mAdapter1: BaseListAdapter<Boolean>? = null,
    private var mAdapter2: BaseListAdapter<Int>? = null
) : BaseMultiListActivity() {

    private  val mHeader: StickHeaderViewBinding by lazy { StickHeaderViewBinding.bind(getView(R.layout.stick_header_view)) }
    private lateinit var mFooter:StickFooterViewBinding

    override fun getLayoutManager()= StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

    override suspend fun initView() {
        super.initView()

        //mHeader = StickHeaderViewBinding.bind(getView(R.layout.stick_header_view))
        mFooter = StickFooterViewBinding.bind(getView(R.layout.stick_footer_view))

        mAdapter0 = addAdapter(R.layout.adapter_multi_0_list_item, convert = { vh, item ->

        })

        mAdapter1 = addAdapter(R.layout.adapter_multi_1_list_item, convert = { vh, item ->

        })

        mAdapter2 = addAdapter(R.layout.adapter_multi_2_list_item, convert = { vh, item ->

        })

        autoRefresh()
        mHeader.headerView.text = "大家好,我是固定头部哈"
        mFooter.footerView.text = "大家好，我是固定尾部哈"
        mAdapter0?.setOnItemClickListener {a, v, position ->onItemClick(a as BaseListAdapter<String>,v,position)  }
        mAdapter1?.setOnItemClickListener {a, v, position ->onItemClick(a as BaseListAdapter<String>,v,position)  }
        mAdapter2?.setOnItemClickListener {a, v, position ->onItemClick(a as BaseListAdapter<String>,v,position)  }
    }


    private fun onItemClick(a: BaseListAdapter<String>, v: View, position: Int) {
        Toast.makeText(this, "点击了 --> $position", Toast.LENGTH_SHORT).show()
    }

    override fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {
        if (isRefresh) {
            it.finishRefresh()
            setList(mAdapter0!!, data())
            setList(mAdapter1!!, dataB())
            setList(mAdapter2!!, dataI())
        } else {
            it.finishLoadMore()
            //it.finishLoadMoreWithNoMoreData()// 关闭加载更多
            addData(mAdapter0!!, data())
            addData(mAdapter1!!, dataB())
            addData(mAdapter2!!, dataI())
        }
    }

    private fun data(): MutableList<String> {
        val data: MutableList<String> = mutableListOf()
        for (i in 0 until 1) {
            data.add("你好")
        }
        return data
    }

    private fun dataB(): MutableList<Boolean> {
        val data: MutableList<Boolean> = mutableListOf()
        for (i in 0 until 1) {
            data.add(true)
        }
        return data
    }

    private fun dataI(): MutableList<Int> {
        val data: MutableList<Int> = mutableListOf()
        for (i in 0 until 1) {
            data.add(i)
        }
        return data
    }

}