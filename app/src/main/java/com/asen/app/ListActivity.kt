package com.asen.app

import android.view.View
import android.widget.Toast
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.StickFooterViewBinding
import com.asen.app.databinding.StickHeaderViewBinding
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListActivity
import com.asen.libcommon.base.BaseListAdapter
import com.asen.libcommon.base.bind.extImmersionBar
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshLayout


/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
@Route(path = ActivityPath.HomeModule.ListActivity)
class ListActivity : BaseListActivity<String>() {

    override fun getItemLayoutId() = R.layout.adapter_list_item

    private lateinit var mHeader:StickHeaderViewBinding
    private lateinit var mFooter:StickFooterViewBinding

    override suspend fun initView() {
        super.initView()
        extImmersionBar(true)
        autoRefresh()
        mHeader = StickHeaderViewBinding.bind(getView(R.layout.stick_header_view))
        mFooter = StickFooterViewBinding.bind(getView(R.layout.stick_footer_view))
        mHeader.headerView.text = "大家好,我是固定头部哈"
        mFooter.footerView.text = "大家好，我是固定尾部哈"
    }

    override fun convert(vh: BaseViewHolder, t: String) {
        vh.setText(R.id.tv_name, "${t}${vh.absoluteAdapterPosition}")
    }

    override fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {
        if (isRefresh) {
            it.finishRefresh()
            setList(data())
        } else {
            it.finishLoadMore()
            //it.finishLoadMoreWithNoMoreData()// 关闭加载更多
            addData(data())
        }
    }

    override fun onItemClick(a: BaseListAdapter<String>, v: View, position: Int) {
        Toast.makeText(this, "点击了 --> $position 位置 = ${a.data[position]}", Toast.LENGTH_SHORT)
            .show()
    }

    private fun data(): MutableList<String> {
        val data: MutableList<String> = mutableListOf()
        for (i in 0 until 30) {
            data.add("你好")
        }
        return data
    }

}