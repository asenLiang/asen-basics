package com.asen.app

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import com.asen.app.databinding.FragmentViewbindBinding
import com.asen.app.info.Produce
import com.asen.app.info.User
import com.asen.app.viewbind.ViewBindModel
import com.asen.libcommon.base.viewbind.BaseViewBindFragment
import com.asen.libcommon.base.viewbind.createViewModel
import com.asen.libcommon.base.viewbind.viewBind

/**
 * @date   : 2021/2/23
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
class ViewBindFragment : BaseViewBindFragment() {

    val TAG: String = "AAA__1"

    override val mBinding: FragmentViewbindBinding by viewBind()

    override val mViewModel: ViewBindModel by createViewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(TAG, "onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.e(TAG, "onCreateView")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override suspend fun initView(view: View) {

        // TODO: 绑定生命周期打印日志
        viewLifecycleOwner.lifecycle.addObserver(LifeFragment())

        // TODO: 控件获取
        mBinding.apply {
            button1.setOnClickListener {
                mViewModel.mUserLiveData.value=User("用户改变拉")
            }

            button2.setOnClickListener {
                mViewModel.mProduceLiveData.value= Produce("产品改变拉")
            }
        }

        // TODO：ViewModel回调
        mViewModel.mUserLiveData.observe(this, Observer {
            mBinding.changeText.text = it.name
        })

        // TODO：ViewModel回调
        mViewModel.mProduceLiveData.observe(this, Observer {
            mBinding.changeText.text = it.name
        })

        (requireActivity() as ViewBindActivity)
            .mViewModel.mUserLiveData.observe(this, Observer {
            mBinding.changeText.text = it.name
        })

        (requireActivity() as ViewBindActivity)
            .mViewModel.mProduceLiveData.observe(this, Observer {
                mBinding.changeText.text = it.name
            })

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.e(TAG, "onActivityCreated")
    }

    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e(TAG, "onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e(TAG, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.e(TAG, "onDetach")
    }

    class LifeFragment : LifecycleObserver {

        companion object {
            private const val TAG: String = "AAA__2"
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onCreate() {
            Log.e(TAG, "onCreate")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
            Log.e(TAG, "onStart")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            Log.e(TAG, "onResume")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            Log.e(TAG, "onPause")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            Log.e(TAG, "onStop")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            Log.e(TAG, "onDestroy")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
        fun onAny() {
            Log.e(Companion.TAG, "onAny")
        }

    }

}