package com.asen.app

import android.annotation.SuppressLint
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.StickFooterChat1Binding
import com.asen.app.databinding.StickFooterViewBinding
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListActivity
import com.asen.libcommon.base.BaseListAdapter
import com.asen.libcommon.base.viewbind.viewBind
import com.asen.libcommon.widget.keyboard.util.KPSwitchConflictUtil
import com.asen.libcommon.widget.keyboard.util.KeyboardUtil
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshLayout

/**
 * @author : asenLiang
 * @date   : 2020/12/24
 * @e-mail : liangAisiSen@163.com
 * @desc   : 普通聊天界面中键盘顶置和隐藏的处理，没有隐藏状态栏（键盘隐藏会闪）
 * 如果觉得麻烦可以集成 BaseMultiItemQuickAdapter 多布局适配器操作
 */
@Route(path = ActivityPath.HomeModule.Chat1Activity)
class Chat1Activity : BaseListActivity<String>() {

    /** 关闭加载更多 */
    override fun openLoadMore() = false

    /** 尾部布局 */
    private  val mFooter : StickFooterChat1Binding by lazy { StickFooterChat1Binding.bind(getView(R.layout.stick_footer_chat1)) }

    /** item布局 */
    override fun getItemLayoutId() = R.layout.adapter_list_item

    override suspend fun initView() {
        super.initView()
        KeyboardUtil.attach(this, mFooter.mPanelContainer)
        KPSwitchConflictUtil.attach(mFooter.mPanelContainer, mFooter.mBtnSend, mFooter.mInputText) { v, switchToPanel ->
            if (switchToPanel) {
                mFooter.mInputText.clearFocus()
            } else {
                mFooter.mInputText.requestFocus()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override suspend fun initEvent() {
        super.initEvent()
        mBinding.mRecyclerView.setOnTouchListener{v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                KPSwitchConflictUtil.hidePanelAndKeyboard(mFooter.mPanelContainer)
            }
            return@setOnTouchListener false
        }

    }

    /** item布局渲染 */
    override fun convert(vh: BaseViewHolder, t: String) {
        vh.setText(R.id.mInputWebsite, t ?: "")
    }

    /** item点击事件监听 */
    override fun onItemClick(a: BaseListAdapter<String>, v: View, position: Int) {
        Toast.makeText(this, "点击了 --> $position 位置 = ${a.data[position]}", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        mFooter.mPanelContainer.recordKeyboardStatus(window)
    }

    /** 监听返回按键退出监听 */
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_UP && event.keyCode == KeyEvent.KEYCODE_BACK && mFooter.mPanelContainer.visibility == View.VISIBLE) {
            KPSwitchConflictUtil.hidePanelAndKeyboard(mFooter.mPanelContainer)
            return true
        }
        return super.dispatchKeyEvent(event)
    }

    /** 当屏幕分屏/多窗口变化时回调 */
    override fun onMultiWindowModeChanged(isInMultiWindowMode: Boolean) {
        super.onMultiWindowModeChanged(isInMultiWindowMode)
        KPSwitchConflictUtil.onMultiWindowModeChanged(isInMultiWindowMode)
    }

    /** 上拉刷新下拉加载监听 */
    override fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {
        if (isRefresh) {
            it.finishRefresh()
            addData(0, "刷新新增")
        }
    }

}