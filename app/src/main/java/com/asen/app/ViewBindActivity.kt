package com.asen.app

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.ActivityViewbindBinding
import com.asen.app.info.Produce
import com.asen.app.info.User
import com.asen.app.router.ActivityPath
import com.asen.app.viewbind.ViewBindModel
import com.asen.libcommon.base.viewbind.BaseViewBindActivity
import com.asen.libcommon.base.viewbind.createViewModel
import com.asen.libcommon.base.viewbind.viewBind

/**
 * @author : asenLiang
 * @date : 2021/2/23
 * @e-mail : liangAisiSen@163.com
 * @desc :
 */
@Route(path = ActivityPath.HomeModule.ViewBindActivity)
class ViewBindActivity : BaseViewBindActivity() {

    override val mBinding: ActivityViewbindBinding by viewBind()

    override val mViewModel: ViewBindModel by createViewModel()

    override suspend fun initView() {

        // TODO: 绑定生命周期打印日志
        lifecycle.addObserver(LifeActivity())

        // TODO: 控件获取
        mBinding.apply {
            button1.setOnClickListener {
                mViewModel.mUserLiveData.value=User("用户改变拉")
            }

            button2.setOnClickListener {
                mViewModel.mProduceLiveData.value= Produce("产品改变拉")
            }
        }

        // TODO：ViewModel回调
        mViewModel.mUserLiveData.observe(this, Observer {
            mBinding.changeText.text = it.name
        })

        // TODO：ViewModel回调
        mViewModel.mProduceLiveData.observe(this, Observer {
            mBinding.changeText.text = it.name
        })

    }

    class LifeActivity : LifecycleObserver {

        companion object {
            private const val TAG: String = "AAA__3"
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onCreate() {
            Log.e(TAG, "onCreate")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onStart() {
            Log.e(TAG, "onStart")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            Log.e(TAG, "onResume")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            Log.e(TAG, "onPause")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            Log.e(TAG, "onStop")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            Log.e(TAG, "onDestroy")
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
        fun onAny() {
            Log.e(Companion.TAG, "onAny")
        }

    }
}