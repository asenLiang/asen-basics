package com.asen.app

import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.ActivityDrawerBinding
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.viewbind.BaseViewBindActivity
import com.asen.libcommon.base.viewbind.viewBind

/**
 * @date   : 2021/3/24
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 抽屉使用activity教程
 */
@Route(path = ActivityPath.HomeModule.DrawerActivity)
class DrawerActivity : BaseViewBindActivity(){

    override val mBinding: ActivityDrawerBinding by viewBind()

    override suspend fun initView() {
        mBinding.apply {
//            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);//关闭手势滑动
//            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);//打开手势滑动
        }
    }
}