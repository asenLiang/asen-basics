package com.asen.app

import android.util.Log
import com.alibaba.android.arouter.launcher.ARouter
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListActivity
import com.asen.libcommon.base.bind.extRefreshFinish
import com.asen.libcommon.base.bind.extBindEventBus
import com.asen.libcommon.base.bind.extImmersionBar
import com.asen.libcommon.widget.CommonButton
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.orhanobut.logger.Logger
import com.scwang.smart.refresh.layout.api.RefreshLayout
import kotlinx.coroutines.*

class MainActivity : BaseListActivity<MainActivity.Item>() {

    override fun getItemLayoutId(): Int = R.layout.activity_main_item

    data class Item(val title:String,val path:String)

    override fun convert(vh: BaseViewHolder, t: Item) {
        vh.getView<CommonButton>(R.id.btnText).apply {
            text = t.title
            setOnClickListener {
                ARouter.getInstance().build(t.path).navigation()
            }
        }
    }

    override suspend fun initView() {
        super.initView()
        mBinding.mSmartRefreshLayout.autoRefresh()
        autoRefresh()
        extImmersionBar(isTransparentStatusBar=false)
        // TODO:EventBus 事件回调
        extBindEventBus {
            Log.e("YYYYYYYYYY", "onEvent")
        }

    }

    override fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {
        if (isRefresh){
            setList(mutableListOf())
            addData(Item("普通聊天键盘处理",ActivityPath.HomeModule.ChatActivity))
            addData(Item("解决冲突聊天布局",ActivityPath.HomeModule.Chat1Activity))
            addData(Item("聊天解决冲突键盘",ActivityPath.HomeModule.ChattingResolvedHandleByPlaceholderActivity))
            addData(Item("列表Activity",ActivityPath.HomeModule.ListActivity))
            addData(Item("列表MultiActivity",ActivityPath.HomeModule.MultiListActivity))
            addData(Item("数据库",ActivityPath.HomeModule.DBActivity))
            addData(Item("ViewBind测试activity/Fragment生命周期",ActivityPath.HomeModule.ViewBindActivity))
            addData(Item("抽屉界面",ActivityPath.HomeModule.DrawerActivity))
            addData(Item("新闻网络请求",ActivityPath.TestModule.NewsActivity))
            addData(Item("ViewPager2使用",ActivityPath.TestModule.ViewPager2Activity))
            addData(Item("列表位置替换和侧滑删除",ActivityPath.TestModule.ItemTouchActivity))
            addData(Item("贝塞尔曲线",ActivityPath.TestModule.BesselCurveActivity))
        }
        it.extRefreshFinish(isRefresh)
    }






    // 携程 runBlocking启动的协程任务会阻断当前线程，直到该协程执行结束。(在onCreate（）方法调用，当协程执行结束之后，页面才会被显示出来。)
    private fun test() = runBlocking {
        Logger.e("主线程id：${mainLooper.thread.id}")
        repeat(8) {
            Logger.e("协程执行$it 线程id：${Thread.currentThread().id}")
        }
        Logger.e("协程执行结束")
    }

    // 携程 TODO 不会阻断主线程
    private suspend fun job() {
        Logger.e("主线程id：${mainLooper.thread.id}")
        val job = GlobalScope.launch {
            delay(6000)
            Logger.e("协程执行结束 -- 线程id：${Thread.currentThread().id}")
        }
        //Job中的方法
        job.isActive
        job.isCancelled
        job.isCompleted
        job.cancel()
        job.join()
        Logger.e("主线程执行结束")

    }

    /**
     * 携程
     * async跟launch的用法基本一样，区别在于：async的返回值是Deferred，将最后一个封装成了该对象。
     * async可以支持并发，此时一般都跟await一起使用，看下面的例子。
     * async是不阻塞线程的,也就是说getResult1和getResult2是同时进行的，所以获取到result的时间是4s，而不是7s。
     */
    private fun async() {
        GlobalScope.launch {
            val result1 = GlobalScope.async {
                getResult1()
            }
            val result2 = GlobalScope.async {
                getResult2()
            }
            val result = result1.await() + result2.await()
            Logger.e("result = $result")
        }
    }

    private suspend fun getResult1(): Int {
        delay(3000)
        return 1
    }

    private suspend fun getResult2(): Int {
        delay(4000)
        return 2
    }


}