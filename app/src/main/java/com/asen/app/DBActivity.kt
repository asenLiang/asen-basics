package com.asen.app

import android.view.View
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.databinding.ActivityDbBinding
import com.asen.app.db.DBTaskBean
import com.asen.app.db.DBUtil
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.viewbind.BaseViewBindActivity
import com.asen.libcommon.base.viewbind.viewBind
import com.luck.picture.lib.tools.ToastUtils


/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
@Route(path = ActivityPath.HomeModule.DBActivity)
class DBActivity(
    private var bean: DBTaskBean = DBTaskBean()
) : BaseViewBindActivity() {

    override val mBinding: ActivityDbBinding by viewBind()

    override suspend fun initView() {
        mBinding.apply {

        }
    }

    fun onInsert(view: View) {
        bean.name = "哈哈"
        ToastUtils.s(this, if (DBUtil<DBTaskBean>().insert(bean)) "成功" else "失败")
    }

    fun onDelete(view: View) {
        DBUtil<DBTaskBean>().delete(bean)
    }

    fun onUpdate(view: View) {
        bean.name = "哈哈1"
        DBUtil<DBTaskBean>().update(bean)
        DBUtil<DBTaskBean>().query(DBTaskBean::class.java,bean.id)
        ToastUtils.s(this, bean.name)
    }

    fun onQuery(view: View) {
        val beanList = DBUtil<DBTaskBean>().queryAll(DBTaskBean::class.java)
        var str=""
        beanList?.forEach {
            str +="\t${it?.name}"
        }
        ToastUtils.s(this, str)
    }

}