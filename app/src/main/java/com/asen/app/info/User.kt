package com.asen.app.info

/**
 * @date   : 2021/2/23
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
data class User(val name: String)