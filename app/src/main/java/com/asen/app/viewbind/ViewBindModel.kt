package com.asen.app.viewbind

import com.asen.app.info.Produce
import com.asen.app.info.User
import com.asen.libcommon.base.viewbind.BaseViewModel


/**
 * @date   : 2021/2/23
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
class ViewBindModel : BaseViewModel() {

    val mUserLiveData = get(User::class.java)

    val mProduceLiveData = get(Produce::class.java)

}