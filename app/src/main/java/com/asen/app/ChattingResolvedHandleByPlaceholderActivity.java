package com.asen.app;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.asen.app.router.ActivityPath;
import com.asen.libcommon.widget.keyboard.KPSwitchFSPanelLinearLayout;
import com.asen.libcommon.widget.keyboard.util.KPSwitchConflictUtil;
import com.asen.libcommon.widget.keyboard.util.KeyboardUtil;


/**
 * Created by Jacksgong on 3/26/16.
 * <p/>
 * For resolving the conflict by showing the panel placeholder.
 * <p/>
 * In case of FullScreen Theme.
 * In case of Translucent Status Theme with the {@code getFitSystemWindow()} is false in root view.
 */
@Route(path = ActivityPath.HomeModule.ChattingResolvedHandleByPlaceholderActivity)
public class ChattingResolvedHandleByPlaceholderActivity extends AppCompatActivity {


    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void adaptTheme(final boolean isFullScreenTheme) {
        if (isFullScreenTheme) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }

    private KPSwitchFSPanelLinearLayout panelRoot;
    private EditText sendEdt;
    private ImageView plusIv;
    private RecyclerView contentRyv;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adaptTheme(false);

        setContentView(R.layout.activity_chatting_fullscreen_resolved);

        contentRyv = (RecyclerView) findViewById(R.id.content_ryv);
        panelRoot = (KPSwitchFSPanelLinearLayout) findViewById(R.id.panel_root);
        sendEdt = (EditText) findViewById(R.id.send_edt);
        plusIv = (ImageView) findViewById(R.id.plus_iv);


        // For present the theme: Translucent Status and FitSystemWindow is True.
        contentRyv.setBackgroundColor(getResources().getColor(R.color.abc_search_url_text_normal));
        // ********* Above code Just for Demo Test, do not need to adapt in your code. ************

        KeyboardUtil.attach(this, panelRoot);
        KPSwitchConflictUtil.attach(panelRoot, plusIv, sendEdt,
                (v, switchToPanel) -> {
                    if (switchToPanel) {
                        sendEdt.clearFocus();
                    } else {
                        sendEdt.requestFocus();
                    }
                });

        contentRyv.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                KPSwitchConflictUtil.hidePanelAndKeyboard(panelRoot);
            }
            return false;
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        panelRoot.recordKeyboardStatus(getWindow());
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_BACK && panelRoot.getVisibility() != View.GONE) {
            KPSwitchConflictUtil.hidePanelAndKeyboard(panelRoot);
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    // 当屏幕分屏/多窗口变化时回调
    @Override
    public void onMultiWindowModeChanged(boolean isInMultiWindowMode) {
        super.onMultiWindowModeChanged(isInMultiWindowMode);
        KPSwitchConflictUtil.onMultiWindowModeChanged(isInMultiWindowMode);
    }


}
