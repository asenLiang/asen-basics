# AsenBasics 框架结构定义


## 定义统一书册书写格式


## 一、模块


### 1、模块命名

 ---> 工具模块 ： library + ModuleName (gradle的头部添加 apply from: "../build.library.gradle")

 ---> 项目模块 ： Module + ModuleName (gradle的头部添加 apply from: "../build.module.gradle")


### 2、模块配置

 ---> 所有 library + ModuleName 模块集成 libraryCommon 公用模块 ，公用模块没有的库可以去 config.gradle 配置文件寻找，没有的库需要按照 config.gradle 的格式添加新库再配置到相应的 LibModule 模块中

 ---> 所有 Module + ModuleName 模块集成 libraryCommon、libraryBase、libraryRes 三个模块，其他模块配置需要自行添加

 ---> 仿照 moduleTest 模块开启 实现模块话单独开发 完成  library 和 module 的转换 (gradle.properties 文件的 isBuildModule = true 为 app 模式)


### 3、模块的 build.gradle 配置（ 可以仿照 libraryCommon 的 build.gradle 配置 ）

 ---> 插件配置：

           plugins {
               id 'com.android.library'
               id 'kotlin-android'
               //id 'kotlin-android-extensions' // 自动识别xml布局的id (不在使用)
               id 'kotlin-kapt'               // 注解器 -> @
           }

 ---> 资源配置：resourcePrefix "moduleName" + "_"  // 设置这里的资源前缀名，防止资源重复报错


### 4、res配置

 ---> library + ModuleName

      1、 所有需要用到的Image图片、xml图片、music音乐、layout布局文件都单独分离，各个模块不相干扰，解决耦合性

 ---> Module + ModuleName

      1、 所有需要用到的Image图片、xml图片、music音乐、layout布局文件都放到 lIBRes模块，防止添加项目重复需要用到的资源

      2、 Module + ModuleName 下只存在layout文件夹 和 values文件夹下的attrs.xml文件

      3、 attrs.xml 文件只包含改项目下封装的控件布局资源和自定义控件的属性


## 二、proguard-rules.pro 文件的混淆配置 [混淆学习](https://www.cnblogs.com/Free-Thinker/p/9707285.html)

 ---> 配个模块都需要定义混淆的配置，在添加文件的时候需要注意哪些文件是需要添加混淆配置的

 ---> activity、receiver、service、content、fragment、application、xxxView控件 不能混淆

 ---> 第三方SDK、native方法和类，js方法、自定义的泛型类 不做混淆，防止使用报错

 ---> 注解类注解方法 不能混淆，枚举中的value和valueOf方法不能混淆


### app 公用的混淆放在 LibBase 中


### library 公用的混淆放在 libraryCommon 中






Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)

