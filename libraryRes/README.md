# libraryRes

#### 介绍

资源模块-只放项目用的资源

res:

     --> drawable ：专门放自定义的 xml 图片 命名格式 xml_自定义

     --> layout   ：项目的公用布局

     --> mipmap   : 放置只是与封装的公用控件有关的资源图片，所有项目的图片不能放在这里

Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)

