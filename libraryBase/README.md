# libraryBase

#### 介绍

## 关联整个项目的核心，可以放一些项目公用的

--> 定义的路径一定要和app的路径一直

java:

     -->db 数据库

     -->router 阿里路由配置

     -->可以封装一些项目公用的控件在这里给其他模块调用

     -->可以封装一些项目公用的api接口

     -->封装 阿里路由公用的所有 key 进行通讯


res:

     --> drawable ：专门放自定义的 xml 图片 命名格式 xml_自定义

     --> layout   ：项目的公用布局

     --> mipmap   : 放置只是与封装的公用控件有关的资源图片，所有项目的图片不能放在这里


## BaseApplication

1、区分模块话初始参数，把集成在哪一个模块的sdk直接在改模块初始化，无需在BaseApplication中复写，导致代码脓肿

2、moduleXXX 下创建一个类 Applicaition 集成 IBaseModule , 在回调的 onInit(application Application) 方法中初始化需要的参数便可

3、每创建一个新的ModuleXXX模板都要把对应的Applicaition配置到 BaseApplicaition 中的 onInit方法里面的列表中进行反射初始化

Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)

