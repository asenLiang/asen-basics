package com.asen.app

import android.app.Application
import com.asen.app.db.DBManager
import com.asen.libcommon.base.IBaseModule
import com.asen.libcommon.config.BaseConfig

/**
 * @date   : 2021/2/7
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 模块话反射调用该类
 */
class Application : IBaseModule {

    override fun onInit(application: Application?): Boolean {
        app = application
        // 初始刷数据库
        DBManager.initDB(app, BaseConfig.sqlName)
        return true
    }

    companion object {
        private var app: Application? = null
        fun getContext(): Application? = app
    }

}