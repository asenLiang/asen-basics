package com.asen.app.db

import android.app.Application
import com.asen.app.db.DaoMaster.DevOpenHelper

/**
 * @date   : 2021/2/7
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : Dao 表的创建和字段定义 （https://www.jianshu.com/p/903fbebf4e66）
 *
 * 注解解释
 *
 * @Entity：告诉GreenDao该对象为实体，只有被@Entity注释的Bean类才能被dao类操作
 * @Id：对象的Id，使用Long类型作为EntityId，否则会报错。(autoincrement = true)表示主键会自增，如果false就会使用旧值
 * @Property：可以自定义字段名，注意外键不能使用该属性
 * @NotNull：属性不能为空
 * @Transient：使用该注释的属性不会被存入数据库的字段中
 * @Unique：该属性值必须在数据库中是唯一值
 * @Generated：编译后自动生成的构造函数、方法等的注释，提示构造函数、方法等不能被修改
 *
 */
object DBManager {
    // 获取表的执行者对表进行增删改查
    var daoSession: DaoSession? = null
        private set

    /**
     * 创建表格
     *
     * @param app    上下文
     * @param dbName 数据库名称
     */
    fun initDB(app: Application?, dbName: String?) {
        //创建数据库mydb.db
        val helper = DevOpenHelper(app, dbName)
        //获取可写数据库
        val database = helper.writableDatabase
        //获取数据库对象
        val daoMaster = DaoMaster(database)
        //获取Dao对象管理者
        daoSession = daoMaster.newSession()
    }

}