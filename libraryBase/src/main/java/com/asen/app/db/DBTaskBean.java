package com.asen.app.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @date   : 2021/2/7
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 任务存储表 ( 注意 ：创建表只能用 Java 语言 ，暂时不支持 kotlin 的注解生成)
 */
@Entity
public class DBTaskBean {

    @Id(autoincrement = true)   // 设置自增长
    private Long id;            // 任务id
    private String name;        // 任务类型名称，根据护理床来分配
    @Generated(hash = 1331898259)
    public DBTaskBean(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    @Generated(hash = 998821618)
    public DBTaskBean() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
