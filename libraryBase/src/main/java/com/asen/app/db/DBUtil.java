package com.asen.app.db;


import java.util.List;
import java.util.Objects;

/**
 * @date   : 2021/2/7
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 对数据库表进行增删改查
 * @param <T> 需要执行的数据库表
 */
public class DBUtil<T> {

    // 数据插入
    public boolean insert(T t) {
        return Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).insert(t) != -1;
    }

    // 插入多条数据
    public boolean insertAll(List<T> list) {
        boolean flag = false;
        try {
            Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).runInTx(() -> {
                for (T t : list) {
                    DBManager.INSTANCE.getDaoSession().insertOrReplace(t);
                }
            });
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    // 修改一条数据
    public void update(T t) {
        Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).update(t);
    }

    // 删除单条记录
    public void delete(T t) {
        Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).delete(t);
    }

    // 删除所有数据
    public void deleteAll(Class<T> cls) {
        Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).deleteAll(cls);
    }

    // 查询所有记录
    public List<T> queryAll(Class<T> cls) {
        return Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).loadAll(cls);
    }

    // 根据主键 ID 查询记录 ：主键必须是 long 属性定义
    public T query(Class<T> cls, long key) {
        return Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).load(cls, key);
    }

    // 使用传统语句查询操作
    public List<T> sqQueryAll(Class<T> cls, String sql, String[] conditions) {
        return Objects.requireNonNull(DBManager.INSTANCE.getDaoSession()).queryRaw(cls, sql, conditions);
    }

}
