package com.asen.app.router.interceptor

import android.content.Context
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.facade.annotation.Interceptor
import com.alibaba.android.arouter.facade.callback.InterceptorCallback
import com.alibaba.android.arouter.facade.template.IInterceptor

/**
 * @date   : 2021/12/21
 * @author : asenLiang
 * @e-mail : 375427684@qq.com
 * @desc   : 登录拦截器
 */

//@Interceptor(priority = 1, name = "登录拦截")
//class LoginInterceptor : IInterceptor {
//
//    override fun process(postcard: Postcard?, callback: InterceptorCallback?) {
//        // 根据获取存取标识判断是否已经登录，如果没有登录直接跳转到登录界面
//    }
//
//    override fun init(context: Context?) {
//
//    }
//
//}