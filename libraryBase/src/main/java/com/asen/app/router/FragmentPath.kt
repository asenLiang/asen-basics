package com.asen.app.router

/**
 * @date   : 2021/12/20
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
object FragmentPath {

    object HomeModule {
        private const val Main = "/home"

    }

    object TestModule{
        private const val Main = "/test"

    }
}