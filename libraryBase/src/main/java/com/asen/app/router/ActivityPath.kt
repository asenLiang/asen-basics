package com.asen.app.router

/**
 * @date   : 2021/3/1
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
object ActivityPath {

    object HomeModule {
        private const val Main = "/home"

        // TODO：普通聊天键盘处理
        const val ChatActivity = "$Main/chat_activity"

        // TODO：解决冲突聊天布局
        const val Chat1Activity = "$Main/Chat1Activity"

        // TODO：聊天解决冲突键盘
        const val ChattingResolvedHandleByPlaceholderActivity = "$Main/ChattingResolvedHandleByPlaceholderActivity"

        // TODO：列表Activity
        const val ListActivity = "$Main/ListActivity"

        // TODO：列表MultiActivity
        const val MultiListActivity = "$Main/MultiListActivity"

        // TODO：数据库
        const val DBActivity = "$Main/DBActivity"

        // TODO：ViewBind测试activity/Fragment生命周期
        const val ViewBindActivity = "$Main/ViewBindActivity"

        // TODO：抽屉界面
        const val DrawerActivity = "$Main/DrawerActivity"


    }

    object TestModule{
        private const val Main = "/test"
        // TODO：新闻界面
        const val NewsActivity = "${Main}/news_activity"

        // TODO：viewPager2
        const val ViewPager2Activity = "${Main}/view_pager2_activity"

        // TODO：ItemTouch
        const val ItemTouchActivity = "${Main}/item_touch_activity"

        // TODO：贝塞尔曲线
        const val BesselCurveActivity = "${Main}/bessel_curve_activity"
    }
}