package com.asen.test.bean
import com.squareup.moshi.JsonClass

import com.squareup.moshi.Json


/**
 * @date   : 2021/3/2
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
@JsonClass(generateAdapter = true)
data class TagBean<DATA>(
    @Json(name = "code")
    val code: String, // 200
    @Json(name = "data")
    val `data`: DATA,
    @Json(name = "extend")
    val extend: Any?, // null
    @Json(name = "message")
    val message: String, // 查询成功
    @Json(name = "remark")
    val remark: String
)

@JsonClass(generateAdapter = true)
data class DataBean(
    @Json(name = "rows")
    val rows: List<Row>,
    @Json(name = "total")
    val total: Int // 5
){
    @JsonClass(generateAdapter = true)
    data class Row(
        @Json(name = "tagId")
        val tagId: Any?, // null
        @Json(name = "tagName")
        val tagName: String, // 热门推荐
        @Json(name = "tagType")
        val tagType: Int // 1
    )
}

