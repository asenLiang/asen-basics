package com.asen.test.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.viewbind.BaseViewBindActivity
import com.asen.libcommon.base.viewbind.viewBind
import com.asen.libcommon.widget.CommonButton
import com.asen.test.R
import com.asen.test.databinding.ActivityBesselCureBinding
import com.asen.test.view.BesselCurve.SpotPraiseView
import com.asen.test.view.BesselCurve.WaveView
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.margin
import org.jetbrains.anko.verticalPadding

/**
 * @author : asenLiang
 * @date   : 2021/9/1
 * @e-mail : liangAisiSen@163.com
 * @desc   : 贝塞尔曲线
 */
@Route(path = ActivityPath.TestModule.BesselCurveActivity)
class BesselCurveActivity : BaseViewBindActivity() {

    override val mBinding: ActivityBesselCureBinding by viewBind()

    private fun getDataList(context: Context): MutableList<View> {
        return mutableListOf<View>().apply {

            add(addBtnView(context, "WaveView") {
                setView(WaveView(context).apply { startAnim() })
            })

            add(addBtnView(context, "MyLoveLayout") {
                setView(SpotPraiseView(context).apply {
                    setOnClickListener{
                        for (i in 0 until 50){
                            addHeart(arrayOf(resources.getDrawable(R.drawable.ic_camera), resources.getDrawable(R.drawable.ic_play_icon), resources.getDrawable(R.drawable.ic_search), resources.getDrawable(R.drawable.icon_back), resources.getDrawable(R.drawable.brvah_sample_footer_loading), resources.getDrawable(R.drawable.icon_file_doc), resources.getDrawable(R.drawable.icon_file_xls)))
                        }
                    }
                })
            })

        }
    }

    override suspend fun initView() {
        mBinding.apply {
            btnContainer.apply {
                getDataList(context).map { addView(it) }
            }
        }
    }

    @SuppressLint("ResourceAsColor", "NewApi")
    private fun addBtnView(
        context: Context, value: String,
        width: Int = LinearLayout.LayoutParams.WRAP_CONTENT,
        height: Int = LinearLayout.LayoutParams.WRAP_CONTENT,
        callback: ((view: View) -> Unit)? = null
    ): CommonButton {
        return CommonButton(context).apply {
            text = value
            setStroke(2f, getColor(R.color.black), getColor(R.color.black), getColor(R.color.black))
            setRadius(40f)
            layoutParams = LinearLayout.LayoutParams(width, height).apply {
                horizontalPadding = 30
                verticalPadding = 10
                margin = 10
            }
            callback?.apply { setOnClickListener(this) }
        }
    }

    private fun <T : View> setView(view: T) {
        mBinding.apply {
            container.apply {
                removeAllViews()
                addView(view.apply {
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                })
            }
        }
    }

}