package com.asen.test.view.pager

import android.view.View
import com.asen.libcommon.base.viewbind.BaseViewBindFragment
import com.asen.libcommon.base.viewbind.viewBind
import com.asen.test.databinding.FragmentDrawer1Binding

/**
 * @date   : 2021/3/25
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 封装ViewPager2的左右侧滑基类
 */
 class Drawer1 : BaseViewBindFragment() {

    override val mBinding: FragmentDrawer1Binding by viewBind()

    override suspend fun initView(view: View) {
        super.initView(view)
        mBinding.apply {

        }
    }

}