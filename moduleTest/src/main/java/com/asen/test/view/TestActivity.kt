package com.asen.test.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.asen.app.router.ActivityPath
import com.asen.libcommon.util.extNavigation
import com.asen.test.R

class TestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }

    fun onClickNew(view: View) {
        extNavigation(ActivityPath.TestModule.NewsActivity) {
            // 传递参数
//            withString(RouterKey.Common.NAME, "asen")
//            withInt(RouterKey.Common.CODE, 1)
        }
    }
}