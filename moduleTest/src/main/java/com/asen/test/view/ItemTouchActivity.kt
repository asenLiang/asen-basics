package com.asen.test.view


import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import com.alibaba.android.arouter.facade.annotation.Route
import com.asen.app.router.ActivityPath
import com.asen.libcommon.base.BaseListActivity
import com.asen.libcommon.base.BaseListAdapter
import com.asen.libcommon.base.bind.extRefreshFinish
import com.asen.libcommon.base.bind.extImmersionBar
import com.asen.test.R
import com.asen.libcommon.widget.recyclerViewTouchHelper.DragRemoveItemTouchHelperCallback
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshLayout
import java.util.*


/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   : item互换位置和左侧滑删除item
 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Route(path = ActivityPath.TestModule.ItemTouchActivity)
class ItemTouchActivity : BaseListActivity<String>() {

    override fun getItemLayoutId() = R.layout.adapter_touch_item

    override suspend fun initView() {
        super.initView()
        extImmersionBar(false)
        autoRefresh()
        ItemTouchHelper(
            DragRemoveItemTouchHelperCallback()
                .apply {
            setCallback(
                { current, target ->
                    // 交换位置
                    Collections.swap(getData(), current, target)
                    getAdapter()?.notifyItemMoved(current,target)
                    return@setCallback true
                },
                {
                    getAdapter()?.removeAt(it)
                }
            )
        }).attachToRecyclerView(mBinding.mRecyclerView)

    }

    override fun onRefreshUpdate(re: RefreshLayout, isRe: Boolean) {
        for (i in 0..10) {
            addData("数据$i")
        }
        re.extRefreshFinish(isRe)
    }

    override fun onItemClick(a: BaseListAdapter<String>, v: View, position: Int) {}

    override fun convert(vh: BaseViewHolder, t: String) {
        vh.setText(R.id.tv_text, t)
    }

}