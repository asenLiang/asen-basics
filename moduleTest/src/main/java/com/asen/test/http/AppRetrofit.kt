package com.asen.test.http

import com.asen.libcommon.http.RetrofitFactory

/**
 * @date   : 2021/2/25
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
object AppRetrofit : RetrofitFactory<AppApi>() {

    // 实例化api请求类
    override fun service(): Class<AppApi> = AppApi::class.java

}