package com.asen.test.http

import androidx.lifecycle.LiveData
import com.asen.test.bean.NewsBean
import com.asen.libcommon.http.info.HttpListResult
import com.asen.libcommon.http.livedata.LiveDataResponse
import com.asen.test.bean.DataBean
import com.asen.test.bean.TagBean
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * @date   : 2021/2/25
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   :
 */
interface AppApi {
    /**
     * Rx格式封装请求
     */
    @GET("/getWangYiNews")
    fun getNews(): Observable<HttpListResult<NewsBean>>

    /**
     * 携程格式封装请求
     * 接口需要加上 [suspend] ！Failed to find the generated JsonAdapter class for class
     * 返回值，直接就是你的数据类型，不需要再包装其他的东西了，超级简介
     */
    @GET("/getWangYiNews")
    suspend fun getNewsBean(): HttpListResult<NewsBean>

    /**
     * LiveData格式封装请求
     */
    @GET("/getWangYiNews")
    fun getLiveDataNews(): LiveData<LiveDataResponse<HttpListResult<NewsBean>>>

    // TODO:获取引流标签列表
    @POST("/api/businessCardArticle/getTagList")
    suspend fun getTagListLive(): TagBean<DataBean>

    // TODO:获取引流标签列表
    @POST("/api/businessCardArticle/getTagList")
    fun getTagList(): Observable<TagBean<DataBean>>

    // TODO:获取引流标签列表
    @POST("/api/businessCardArticle/getTagList")
    fun getTagListAA(): LiveData<LiveDataResponse<TagBean<DataBean>>>

    // TODO：检查app是否有更新（蒲公英文档）
    @POST("/apiv2/app/check")
    fun check(@Body body: RequestBody): LiveData<Any>

}