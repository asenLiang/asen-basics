package com.asen.test

import android.app.Application
import com.asen.libcommon.base.IBaseModule

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 测试模块 application 初始化
 */
class Application : IBaseModule {

    override fun onInit(application: Application?): Boolean {
        app = application
        return true
    }

    companion object {
        private var app: Application? = null
        fun getContext(): Application? = app
    }

}