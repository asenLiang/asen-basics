# ↑ ↓混淆配置规则说明：https://www.jianshu.com/p/0d1b58e06796
# ↑ ↓混淆配置规则说明：https://blog.csdn.net/guolipeng_network/article/details/74551968



####################################################################################################
#============================================系统混淆配置============================================
#↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓




#---------------------------------------------------------------------------------------------------
# 指定代码的压缩级别 代码混淆压缩比，在0~7之间，默认为5，一般不做修改
-optimizationpasses 5
# 指定不去忽略非公共的库的类的成员
-dontskipnonpubliclibraryclassmembers
# 不进行预校验,Android不需要,可加快混淆速度。
-dontpreverify
# 混淆时采用的算法
# -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
# 把混淆类中的方法名也混淆了
-useuniqueclassmembernames
# 优化时允许访问并修改有修饰符的类和类的成员=>指示语：不能用这个指令处理库中的代码，
# 因为有的类和类成员没有设计成public ,而在api中可能变成public
-allowaccessmodification
# 屏蔽警告
-ignorewarnings
# 混淆时不使用大小写混合类名
-dontusemixedcaseclassnames
# 不跳过library中的非public的类
-dontskipnonpubliclibraryclasses
# 打印混淆的详细信息
-verbose
# 不进行优化，建议使用此选项，
-dontoptimize



#---------------------------------------------------------------------------------------------------
## 记录生成的日志数据,gradle build时在本项目根目录输出##
# apk 包内所有 class 的内部结构
-dump class_files.txt
# 未混淆的类和成员
-printseeds seeds.txt
# 列出从apk中删除的代码
-printusage unused.txt
# 混淆前后的映射
-printmapping mapping.txt

# 保存注解参数
-keepattributes *Annotation*
-keepattributes Exceptions
# 保留Annotation不混淆 这在JSON实体映射时非常重要，比如fastJson
-keepattributes *Annotation*,InnerClasses
# 保持泛型
-keepattributes Signature
# 并保留源文件名为"Proguard"字符串，而非原始的类名 并保留行号
-keepattributes SourceFile,LineNumberTable




#↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
#=============================================系统混淆配置===========================================
####################################################################################################










####################################################################################################
#==============================================默认保留区============================================
#↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓




#---------------------------------------------------------------------------------------------------
# JS接口类不混淆，否则执行不了
-dontwarn com.android.JsInterface.**
-keep class com.android.JsInterface.** {*; }



#---------------------------------------------------------------------------------------------------
# 保留Google原生服务需要的类
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService



#---------------------------------------------------------------------------------------------------
# 保留native方法的类名和方法名
-keepclasseswithmembernames class * { native <methods>; }



#---------------------------------------------------------------------------------------------------
# R文件中的所有记录资源id的静态字段
-keepclassmembers class **.R$* { public static <fields>; }
# 保留R下面的资源
-keep class **.R$*{*;}



#---------------------------------------------------------------------------------------------------
# 保持所有实现 Serializable 接口的类成员
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}



#---------------------------------------------------------------------------------------------------
# Parcelable实现类中的CREATOR字段是绝对不能改变的，包括大小写
-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
# 保持Parcelable不被混淆
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}



#---------------------------------------------------------------------------------------------------
# 保留混淆枚举中的values()和valueOf()方法
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}



#---------------------------------------------------------------------------------------------------
# 对于带有回调函数的onXXEvent、**On*Listener的，不能被混淆
-keepclassmembers class * {
    void *(**On*Event);
    void *(**On*Listener);
}



#---------------------------------------------------------------------------------------------------
# 保持测试相关的代码
-dontnote junit.framework.**
-dontnote junit.runner.**
-dontwarn android.test.**
-dontwarn android.support.test.**
-dontwarn org.junit.**



#---------------------------------------------------------------------------------------------------
# 四大组件相关不能混淆
-keep public class * extends androidx.appcompat.app.AppCompatActivity
-keep public class * extends androidx.fragment.app.Fragment
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
# 继承 v4 和 v7 包不混淆
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
# 自定义View构造方法不混淆
-keep public class * extends android.view.View
-keep public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
# 保持自定义控件类不被混淆
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
# 保留Activity中参数是View的方法，如XML中配置android:onClick=”buttonClick”属性，
# Activity中调用的buttonClick(View view)方法
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
# 保留自定义View,如"属性动画"中的set/get方法
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}
# 忽略support包因为版本兼容产生的警告
-dontwarn android.support.**
# Design包不混淆
-dontwarn android.support.design.**
-keep class android.support.design.**{*;}
-keep interface android.support.design.**{*;}
-keep public class android.support.design.R$*{*;}
# 注解 不混淆.
-keep class android.support.annotation.Keep
-keep public class * extends android.support.annotation.**
-keep @android.support.annotation.Keep class *{*;}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
-keepclassmembers class * {
    @android.support.annotation.Keep *;
}



#---------------------------------------------------------------------------------------------------
# kotlin库的混淆
-keep class kotlin.**{*;}
-keep class kotlin.Metadata{*;}
-dontwarn kotlin.**
-keepclassmembers class **$WhenMappings { <fields>; }
-keepclassmembers class kotlin.Metadata { public <methods>; }
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}



#---------------------------------------------------------------------------------------------------
# 忽略androidx包因为版本兼容产生的警告
-dontwarn androidx.**
# 忽略google的material包因为版本兼容产生的警告
-dontwarn com.google.android.material.**
-dontnote com.google.android.material.**
-keep class com.google.android.material.**{*;}
-keep class androidx.**{*;}
-keep public class * extends androidx.**
-keep interface androidx.**{*;}
#androidx注解不混淆
-keepclassmembers class * { @androidx.annotation.Keep *; }
#保持所有databinding类
-dontwarn androidx.databinding.**
-dontnote androidx.databinding.**
-dontnote android.databinding.**
-keep class androidx.databinding.**{*;}
-keep class androidx.annotation.**{*;}




#↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
#==============================================默认保留区============================================
####################################################################################################










####################################################################################################
#===============================================依赖库混淆===========================================
#↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓




#---------------------------------------------------------------------------------------------------
# 状态栏不能混淆
-keep class com.gyf.immersionbar.*{*;}
-dontwarn com.gyf.immersionbar.**



#---------------------------------------------------------------------------------------------------
# xpopup
-dontwarn com.lxj.xpopup.widget.**
-keep class com.lxj.xpopup.widget.**{*;}



#---------------------------------------------------------------------------------------------------
# 今日头条的autosize
-keep class me.jessyan.autosize.**{*;}
-keep interface me.jessyan.autosize.**{*;}



#---------------------------------------------------------------------------------------------------
# leakcanary 内存溢出安全检测
-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.**{*;}
-keep class org.eclipse.mat.**{*;}
-keep class com.squareup.leakcanary.**{*;}



#---------------------------------------------------------------------------------------------------
# eventBus
-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode{*;}
# And if you use AsyncExecutor:
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}



#---------------------------------------------------------------------------------------------------
# Glide
-dontwarn com.bumptech.glide.**
-keep class com.bumptech.glide.**{*;}
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * implements com.bumptech.glide.module.AppGlideModule
-keep public class * implements com.bumptech.glide.module.LibraryGlideModule
-keep class * extends com.bumptech.glide.module.AppGlideModule { <init>(...); }
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
  *** rewind();
}



#---------------------------------------------------------------------------------------------------
# gson 和 json 不混淆
-dontwarn com.google.**
-keep class com.google.gson.stream.**{*;}
-keepattributes EnclosingMethod
-keep class sun.misc.Unsafe{*;}
-keep class com.google.**{*;}
-keep class com.google.gson.**{*;}
-keep class com.google.gson.stream.**{*;}
-keep class com.google.gson.examples.android.model.**{*;}
-keep class com.google.gson.JsonObject{*;}
-keep class com.futurice.project.models.pojo.**{*;}
-keep class * extends com.badlogic.gdx.utils.Json*
-keep class com.badlogic.**{*;}
-keep class org.json.**{*;}
-keepclassmembers class * { public <init> (org.json.JSONObject); }
-keepclassmembers @com.squareup.moshi.JsonClass class * extends java.lang.Enum {
    <fields>;
    **[] values();
}



#---------------------------------------------------------------------------------------------------
# Retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.**{*;}
-keepattributes Signature
-keepattributes Exceptions
-dontwarn org.robovm.**
-keep class org.robovm.**{*;}



#---------------------------------------------------------------------------------------------------
# OkHttp3
-dontwarn com.squareup.okhttp3.**
-dontwarn com.squareup.okhttp.**
-dontwarn okhttp3.logging.**
-dontwarn okhttp3.**
-keep class okhttp3.*{*;}
-keep class okhttp3.internal.**{*;}
-keep class com.squareup.okhttp3.**{*;}
-keep class com.squareup.okhttp.**{*;}
-keep interface com.squareup.okhttp.**{*;}
-dontwarn okio.**
-keep class okio.**{*;}
-keep interface okio.**{*;}
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.security.*
-keep class sun.security.**{*;}



#---------------------------------------------------------------------------------------------------
# RxJava RxAndroid
-dontwarn rx.*
-dontwarn sun.misc.**
-dontwarn java.util.concurrent.Flow*
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
-dontwarn org.mockito.**
-dontwarn org.junit.**
-dontwarn org.robolectric.**
-keep class io.reactivex.**{*;}
-keep interface io.reactivex.**{*;}
-dontwarn io.reactivex.**
-dontwarn retrofit.**
-keep class retrofit.**{*;}
-keepclasseswithmembers class * { @retrofit.http.* <methods>; }
-dontwarn java.lang.invoke.*
-keep class io.reactivex.schedulers.Schedulers {  public static <methods>; }
-keep class io.reactivex.schedulers.ImmediateScheduler { public <methods>; }
-keep class io.reactivex.schedulers.TestScheduler { public <methods>; }
-keep class io.reactivex.schedulers.Schedulers { public static ** test(); }
-keepclassmembers class io.reactivex.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class io.reactivex.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    long producerNode;
    long consumerNode;
    io.reactivex.internal.util.atomic.LinkedQueueNode producerNode;
    io.reactivex.internal.util.atomic.LinkedQueueNode consumerNode;
}
-dontwarn io.reactivex.internal.util.unsafe.**



#---------------------------------------------------------------------------------------------------
# 万能适配器
-keep public class * extends com.chad.library.adapter.base.viewholder.BaseViewHolder{
 <init>(...);
}
-keepclassmembers  class **$** extends com.chad.library.adapter.base.viewholder.BaseViewHolder {
     <init>(...);
}



#---------------------------------------------------------------------------------------------------
# webviewlib
-dontwarn com.tencent.**
-keep class com.ycbjie.webviewlib.**{*;}
-keep class com.tencent.**{*;}
-keepclassmembers class fqcn.of.javascript.interface.for.webview { public *;}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
    public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String);
}



#---------------------------------------------------------------------------------------------------
# 阿里路由 ARouter
-keep public class com.alibaba.android.arouter.routes.**{*;}
-keep public class com.alibaba.android.arouter.facade.**{*;}
-keep class * implements com.alibaba.android.arouter.facade.template.ISyringe{*;}
# 如果使用了 byType 的方式获取 Service，需添加下面规则，保护接口
-keep interface * implements com.alibaba.android.arouter.facade.template.IProvider
# 如果使用了 单类注入，即不定义接口实现 IProvider，需添加下面规则，保护实现
# -keep class * implements com.alibaba.android.arouter.facade.template.IProvider



#---------------------------------------------------------------------------------------------------
# 腾讯bugly
-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}



#↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
#===============================================依赖库混淆===========================================
####################################################################################################









####################################################################################################
#=============================================导入aar或jar混淆=======================================
#↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓




#↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
#=============================================导入aar或jar混淆=======================================
####################################################################################################










####################################################################################################
#=============================================自己写的代码区域=======================================
#↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓




#---------------------------------------------------------------------------------------------------
# 自定义控件不混淆代码
-keep class com.asen.libcommon.widget.**{*;}
# 网络请求封装
-keep class com.asen.libcommon.http.exception.*{*;}
-keep class com.asen.libcommon.http.info.*{*;}
-keep class com.asen.libcommon.http.RetrofitFactory{*;}




#↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
#=============================================自己写的代码区域=======================================
####################################################################################################





