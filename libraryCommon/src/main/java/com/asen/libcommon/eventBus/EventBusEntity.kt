package com.asen.libcommon.eventBus

import java.io.Serializable

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : EventBus实体类
 */
data class EventBusEntity(val tag: String? = "", val result: Any? = "") : Serializable