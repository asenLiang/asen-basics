package com.asen.libcommon.eventBus

import org.greenrobot.eventbus.EventBus


/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : EventBus 工具类
 */
object EventBusUtil {

    /**
     * 注册EventBus
     */
    fun register(subscriber: Any?) {
        if (!EventBus.getDefault().isRegistered(subscriber))
            EventBus.getDefault().register(subscriber)
    }

    /**
     * 销毁EventBus
     */
    fun unregister(subscriber: Any?) {
        EventBus.getDefault().unregister(subscriber)
    }

    /**
     * 发送EventBus
     */
    fun sendEvent(event: EventBusEntity?) {
        EventBus.getDefault().post(event)
    }

    /**
     * 发送EventBus
     */
    fun sendEvent(tag: String, result: Any? = null) {
        EventBus.getDefault().post(EventBusEntity(tag, result))
    }

    /**
     * 发送粘性EventBus
     */
    fun sendStickyEvent(event: EventBusEntity?) {
        EventBus.getDefault().postSticky(event)
    }

    /**
     * 发送粘性EventBus
     */
    fun sendStickyEvent(tag: String, result: Any? = null) {
        EventBus.getDefault().postSticky(EventBusEntity(tag, result))
    }

    /**
     *  判断是否注册
     */
    fun isRegistered(subscriber: Any?): Boolean {
        return EventBus.getDefault().isRegistered(subscriber)
    }

}