package com.asen.libcommon.http

/**
 * @date   : 2021/1/19
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : http 请求返回状态码
 */
object HttpState {

    /** 成功 */
    const val success = 200

    /** token过期 */
    const val tokenInvalid = 401

    /** 服务器失效（没有该服务器），访问不了 */
    const val serviceInvalid = 404

    /** 服务器异常 */
    const val serviceError = 500


    /** */

    /** */
    /** */

}