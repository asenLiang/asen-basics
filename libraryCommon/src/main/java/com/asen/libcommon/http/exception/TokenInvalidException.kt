package com.asen.libcommon.http.exception

import com.asen.libcommon.http.HttpState

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : token失效异常
 */
class TokenInvalidException(code: Int = HttpState.tokenInvalid, message: String = "token失效") :
    BaseException(code, message)