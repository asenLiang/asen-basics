package com.asen.libcommon.http.exception

import com.asen.libcommon.http.HttpState

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : token失效异常
 */
class ServiceInvalidException(code: Int = HttpState.serviceInvalid, message: String = "服务报错404") :
    BaseException(code, message)