package com.asen.libcommon.http.exception

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 异常基类封装
 */
open class BaseException(val code: Int, override val message: String) : Error()