package com.asen.libcommon.http.interceptor

import com.asen.libcommon.http.HttpState
import com.asen.libcommon.http.exception.ServiceErrorException
import com.asen.libcommon.http.exception.ServiceInvalidException
import com.asen.libcommon.http.exception.TokenInvalidException
import com.orhanobut.logger.Logger
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 错误拦截器
 */
class ErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request())

        Logger.v("返回状态---->:${response.code()}")

        when (response.code()) {

            // 成功
            // HttpState.success -> { }

            // 统一拦截token失效后启动登录界面，杀死必要的程序，如清空登录信息，杀死聊天进程等
            HttpState.tokenInvalid -> {
                //throw TokenInvalidException()   // TODO: 使用 LiveData 或 携程 会闪退回上一界面，如果要用，建议使用RXJava做接口封装
                Logger.e("token失效")
            }

            // 服务器失效（没有该服务器），访问不了
            HttpState.serviceInvalid -> {
                //throw ServiceInvalidException() // TODO: 使用 LiveData 或 携程 会闪退回上一界面，如果要用，建议使用RXJava做接口封装
                Logger.e("服务报错404")
            }

            // 服务异常
            HttpState.serviceError -> {
                //throw ServiceErrorException()   // TODO: 使用 LiveData 或 携程 会闪退回上一界面，如果要用，建议使用RXJava做接口封装
                Logger.e("服务报错500")
            }

        }

        return response

    }

}