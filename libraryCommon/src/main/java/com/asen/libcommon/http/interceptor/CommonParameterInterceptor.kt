package com.asen.libcommon.http.interceptor

import com.asen.libcommon.http.HttpConfig
import com.asen.libcommon.util.SPUtil
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 公用参数拦截器 ( post请求状态下 )
 */
class CommonParameterInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val request: Request = chain.request()

        return when (request.method()) {
            "post", "POST" -> {

                val modifiedUrl = request.url().newBuilder() // 在此处提供自定义参数
                    .addQueryParameter("token", SPUtil.getString(HttpConfig.token, ""))
                    .addQueryParameter("channel", HttpConfig.channel) // 添加渠道信息（多渠道打包的标识）
                    //.addQueryParameter("versionName", "")    // 版本名称
                    //.addQueryParameter("versionCode", "")    // 版本code
                    .build()

                chain.proceed(request.newBuilder().url(modifiedUrl).build())

            }
            else -> chain.proceed(request)
        }

    }

}