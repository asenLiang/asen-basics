package com.asen.libcommon.http.interceptor

import com.asen.libcommon.http.HttpConfig
import com.asen.libcommon.util.SPUtil
import com.orhanobut.logger.Logger
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response


/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 本地 baseUrl 正式测试环境/第三方url 替换拦截器
 */
class LocalBaseUrlInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        // 获取原始的originalRequest
        val request = chain.request()

        // 获取老的url
        val oldUrl: HttpUrl = request.url()

        // 获取originalRequest的创建者builder
        val builder = request.newBuilder()

        // 获取 sp 存储得 baseURL,没有保存默认是 ""  -> 保存项目服务url
        val spUrl = SPUtil.getString(HttpConfig.baseUrlKey, "")

        // 获取头信息的集合如（保存第三方url）
        val urlList: List<String?>? = request.headers(HttpConfig.baseUrlKey)
        val baseUrl = if (urlList != null && urlList.isNotEmpty()) urlList[0] ?: spUrl else spUrl

        Logger.v("请求环境 ---> ${baseUrl ?: oldUrl}")

        return if (baseUrl != null && baseUrl.trim().isNotEmpty()) {

            // 删除原有配置中的值,就是namesAndValues集合里的值
            builder.removeHeader(HttpConfig.baseUrlKey)

            // 获取头信息中配置的value,如：manage或者mdffx
            val httpUrl: HttpUrl? = HttpUrl.parse(baseUrl)

            // 重建新的HttpUrl，需要重新设置的url部分
            val newHttpUrl = oldUrl.newBuilder()
                .scheme(httpUrl!!.scheme()) // http协议如：http或者https
                .host(httpUrl.host())       // 主机地址
                .port(httpUrl.port())       // 端口
                .build()

            // 获取处理后的新newRequest
            val newRequest = builder.url(newHttpUrl).build()

            chain.proceed(newRequest)

        } else chain.proceed(request)

    }

}