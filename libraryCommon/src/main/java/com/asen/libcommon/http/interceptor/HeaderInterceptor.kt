package com.asen.libcommon.http.interceptor

import com.asen.libcommon.http.HttpConfig
import com.asen.libcommon.util.SPUtil
import com.orhanobut.logger.Logger
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 头部拦截器 添加全局公用的 头部拦截参数
 */
class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()

        val builder = request.newBuilder()

        val token = SPUtil.getString(HttpConfig.token, "")
        builder.addHeader(HttpConfig.token, token ?: "")
        builder.addHeader("Content-type", "application/json; charset=utf-8")

        Logger.v("token---->:$token")

        return chain.proceed(builder.build())

    }

}