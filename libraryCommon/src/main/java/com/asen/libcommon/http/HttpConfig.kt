package com.asen.libcommon.http

/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 网络请求配置
 */
object HttpConfig {

    /** 测试环境 */
    var debugUrl = "https://api.apiopen.top" // TODO:开源社区的接口，可直接进入查看需要什么接口进行调试

    /** 正式环境 */
    val releaseUrl = ""

    /**
     * 第三方 baseUrl 配置 key ( 在对应的请求方法中使用头部注释拼接 @header("${baseUrlKey}:${url}") ,
     * 拦截器直接获取 url 再次设置赋值给 Retrofit 请求 ;)
     */
    const val baseUrlKey = "BASE_URL_KEY"

    /** 设置请求时间 */
    const val DEFAULT_TIMEOUT: Long = 60

    /** 50M 的缓存大小 */
    const val MAX_CACHE_SIZE: Long = 1024 * 1024 * 50

    /** 头部注解key */
    const val token = "token"

    /** 多渠道标识 */
    const val channel = ""

    /** Cookie 网络请求获取到的 */
    const val Cookie = "Set-Cookie"
    const val CookieKey = "Cookies"


}