package com.asen.libcommon.http.info

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 请求结果返回基类封装，根据公司实际境况修改即可
 */
@JsonClass(generateAdapter = true)
open class BaseResponse(
    /** 返回的消息 */
    @Json(name = "message")
    val message: String = "",
    /** 返回的备注(描述) */
    @Json(name = "remark")
    val remark: String = "",
    /** 返回状态 */
    @Json(name = "code")
    val code: String = ""
)