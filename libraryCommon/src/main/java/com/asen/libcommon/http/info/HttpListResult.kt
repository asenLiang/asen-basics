package com.asen.libcommon.http.info

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 返回请求列表状态的数据
 */
@JsonClass(generateAdapter = true)
data class HttpListResult<DATA>(
    @Json(name = "result")
    val result: MutableList<DATA> = mutableListOf()
) : BaseResponse()