package com.asen.libcommon.http.interceptor

import com.asen.libcommon.http.HttpConfig
import com.asen.libcommon.util.SPUtil
import okhttp3.Interceptor
import okhttp3.Response


/**
 * @date   : 2021/1/18
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : Cookie 拦截器设置
 */
class CookieInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request())

        // 这里获取请求返回的cookie
        if (response.headers(HttpConfig.Cookie).isNotEmpty()) {

            val cookies: MutableSet<String> = mutableSetOf()

            for (header in response.headers(HttpConfig.Cookie)) {
                cookies.add(header)
            }

            SPUtil.put(HttpConfig.CookieKey, cookies)

        }

        return response

    }


}