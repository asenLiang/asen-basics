package com.asen.libcommon.http.exception

import com.asen.libcommon.http.HttpState

/**
 * @date   : 2021/1/22
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : token失效异常
 */
class ServiceErrorException(code: Int = HttpState.serviceError, message: String = "服务报错500") : BaseException(code, message)