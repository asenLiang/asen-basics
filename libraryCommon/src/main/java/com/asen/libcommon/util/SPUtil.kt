package com.asen.libcommon.util

import android.annotation.SuppressLint
import android.content.Context

/**
 * @date   : 2021/1/15
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : SharedPreferences 存储
 */
object SPUtil {

    const val PREFERENCES_FILE = "asen_sp"

    @SuppressLint("StaticFieldLeak")
    private lateinit var sContext: Context

    fun init(ctx: Context) {
        sContext = ctx
    }

    fun put(key: String, value: String) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putString(key, value).apply()
    }

    fun put(key: String, value: Boolean) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putBoolean(key, value).apply()
    }

    fun put(key: String, value: Float) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putFloat(key, value).apply()
    }

    fun put(key: String, value: Int) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putInt(key, value).apply()
    }

    fun put(key: String, value: Long) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putLong(key, value).apply()
    }

    fun put(key: String, value: MutableSet<String>) {
        sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .putStringSet(key, value).apply()
    }


    fun getString(key: String, defValue: String): String? {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getString(key, defValue)
    }

    fun getBoolean(key: String, defValue: Boolean): Boolean {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getBoolean(key, defValue)
    }

    fun getFloat(key: String, defValue: Float): Float {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getFloat(key, defValue)
    }

    fun getInt(key: String, defValue: Int): Int {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getInt(key, defValue)
    }

    fun getLong(key: String, defValue: Long): Long {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getLong(key, defValue)
    }

    fun getHashSet(key: String, value: MutableSet<String>): MutableSet<String>? {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
            .getStringSet(key, value)
    }

    operator fun contains(key: String): Boolean {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).contains(key)
    }

    fun remove(key: String): Boolean {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit()
            .remove(key).commit()
    }

    fun clear(): Boolean {
        return sContext.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).edit().clear()
            .commit()
    }

}