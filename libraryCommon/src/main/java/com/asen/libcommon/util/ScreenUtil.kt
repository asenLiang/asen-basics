package com.asen.libcommon.util

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.util.DisplayMetrics
import android.view.WindowManager


/**
 * @date   : 2020/12/11
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 屏幕处理工具
 */
object ScreenUtil {

    /**
     * dp转px
     */
    fun dp2px(context: Context, dipValue: Float): Int {
        return (dipValue * getDensity(context) + 0.5f).toInt()
    }

    /**
     * px转dp
     */
    fun px2dip(context: Context, pxValue: Float): Int {
        return (pxValue / getDensity(context) + 0.5f).toInt()
    }

    /**
     * sp转px
     */
    fun sp2px(context: Context, spValue: Float): Int {
        return (spValue * getScaleDensity(context) + 0.5f).toInt()
    }

    /**
     * 像素密度
     */
    private fun getDensity(context: Context): Float {
        return context.applicationContext.resources.displayMetrics.density
    }

    /**
     * 缩放比率密度（一般用于字体）
     */
    private fun getScaleDensity(context: Context): Float {
        return context.applicationContext.resources.displayMetrics.scaledDensity
    }

    /**
     * 获取全屏宽
     */
    fun getScreenWidth(context: Context): Int {
        val wm =
            context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = Point()
        wm.defaultDisplay.getSize(p)
        return p.x
    }

    /**
     * 获取屏高
     */
    fun getScreenHeight(context: Context): Int {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = Point()
        wm.defaultDisplay.getSize(p)
        return p.y
    }

    /**
     * 获取全屏高（状态栏高+屏幕高+导航栏高）
     * 只能传递activity来获取屏幕全局高
     */
    fun getDefaultScreenHeight(context: Activity): Int {
        val dm = DisplayMetrics()
        context.windowManager.defaultDisplay.getRealMetrics(dm)
        return dm.heightPixels
    }

    /**
     * 获取导航栏高度
     */
    fun getNavBarHeight(context: Context): Int {
        val resources = context.resources
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return when {
            resourceId > 0 -> {
                resources.getDimensionPixelSize(resourceId)
            }
            context is Activity -> {
                getDefaultScreenHeight(context) - getScreenHeight(context)
            }
            else -> 0
        }
    }

    /**
     * 获取状态栏高度
     */
    fun getStatusBarHeight(context: Context): Int {
        var statusbarheight = 0
        if (statusbarheight == 0) {
            try {
                val c = Class.forName("com.android.internal.R\$dimen")
                val o = c.newInstance()
                val field = c.getField("status_bar_height")
                val x = field[o] as Int
                statusbarheight =
                    context.resources.getDimensionPixelSize(x)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if (statusbarheight == 0) {
            statusbarheight = dp2px(context, 25f)
        }
        return statusbarheight
    }

}