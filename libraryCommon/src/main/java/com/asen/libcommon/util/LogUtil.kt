package com.asen.libcommon.util

import com.asen.libcommon.config.BaseConfig
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy

/**
 * @date   : 2021/2/7
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 日志输出，防止其他库有 Logger 类导包出错，直接封装使用
 */
object LogUtil {

    /** 初始化日志 */
    fun initLogger() {
        Logger.addLogAdapter(object : AndroidLogAdapter(
            PrettyFormatStrategy.newBuilder()
                .showThreadInfo(true) // 是否显示线程信息，默认为true
                .methodCount(1)       // 显示的方法行数，默认为2
                .methodOffset(0)      // 隐藏内部方法调用到偏移量，默认为5
                .tag("asenLog")          // 每个日志的全局标记。默认PRETTY_LOGGER
                .build()
        ) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BaseConfig.isDebug  // 关闭打印日志设置为false TODO:设置是否打印日志
            }
        })
    }

}