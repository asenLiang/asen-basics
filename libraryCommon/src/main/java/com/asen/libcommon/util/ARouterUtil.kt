package com.asen.libcommon.util

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.launcher.ARouter

/**
 * @date   : 2021/12/21
 * @author : asenLiang
 * @e-mail : 375427684@qq.com
 * @desc   :
 */


/** 跳转方式 */
fun Context.extNavigation(path: String, requestCode: Int? = null, post: (Postcard.() -> Unit)? = null) {
    val postcard = ARouter.getInstance().build(path)
    post?.invoke(postcard)
    if (requestCode != null && this is Activity) {
        postcard.navigation(this, requestCode)
    } else {
        postcard.navigation(this)
    }
}

/** 跳转方式 */
fun Fragment.extNav(path: String, post: (Postcard.() -> Unit)? = null) {
    val postcard = ARouter.getInstance().build(path)
    post?.invoke(postcard)
    postcard.navigation(requireContext())
}