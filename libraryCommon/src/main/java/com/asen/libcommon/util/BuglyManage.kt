package com.asen.libcommon.util

import android.app.Application
import android.content.Context
import com.asen.libcommon.BuildConfig
import com.tencent.bugly.crashreport.CrashReport

/**
 * @author Lin
 * @time 2021/4/14 15:52
 *
 * @desc bugly管理工具
 */

object BuglyManage {

    fun initBugly(context: Context) {
        val appId = when (1) {
            //val appId = when (HttpConstant.BASE_URL) {
//            HttpConstant.URL_PRODUCTION -> "5d3a6348dd"  //正式环境
//            HttpConstant.URL_DEVELOP->"e32036588d" //开发环境
//            HttpConstant.URL_TEST->"9304ae476e" //测试环境
            else -> "e32036588d" //开发环境
        }
        CrashReport.initCrashReport(context, appId, BuildConfig.DEBUG);
        postData(context)
    }

    private fun postData(context: Context) {
//        val userId = UserConfig.getResult()?.id.extValueNull() ?: "UserID 暂时为空"
//        val cardId = UserConfig.getResult()?.card?.id.extValueNull() ?: "CardId 暂时为空"
//        val phone = UserConfig.getResult()?.phone.extValueNull() ?: "电话号码 暂时为空"
//        val userName = UserConfig.getResult()?.username.extValueNull() ?: "用户名 暂时为空"
//        val cardRealName = UserConfig.getResult()?.card?.realName.extValueNull() ?: "名片用户名 暂时为空"
//        CrashReport.putUserData(context, "UserId", userId)
//        CrashReport.putUserData(context, "CardId", cardId)
//        CrashReport.putUserData(context, "Phone", phone)
//        CrashReport.putUserData(context, "UserName", userName)
//        CrashReport.putUserData(context, "Card.RealName", cardRealName)
    }

    /**
     * 发送自定义Bugly信息
     */
    fun putUserData(application: Application, tag: String, text: String) {
        CrashReport.putUserData(application, tag, text)
    }

}