package com.asen.libcommon.util

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.Format


/**
 * @author : asenLiang
 * @date   : 2021/03/03
 * @e-mail : liangAisiSen@163.com
 * @desc   : 格式化数字
 */
object NumberUtil {

    /**
     * 返回带‘万’的数字
     */
    fun formatWan(targetInt: Int, isNeedNewLine: Boolean): String {
        var value = targetInt
        return if (value > 9999) {
            val text = targetInt.toString()
            val text1 = text.substring(0, text.length - 4)
            val text2 = text.substring(text.length - 4)
            if (isNeedNewLine) "${text1}万$text2".trimIndent() else text1 + "万" + text2
        } else if (value > -1 && value <= 9999) {
            targetInt.toString()
        } else if (value <= -1 && value >= -9999) {
            targetInt.toString()
        } else {
            value = Math.abs(value)
            val text = value.toString()
            val text1 = text.substring(0, text.length - 4)
            val text2 = text.substring(text.length - 4)
            if (isNeedNewLine) "-${text1}万$text2".trimIndent() else "-" + text1 + "万" + text2
        }
    }

    /**
     * 保留两位小数
     */
    fun formatDouble(targetDouble: Double): String {
        val decimalFormat = DecimalFormat("#########0.00")
        return decimalFormat.format(targetDouble)
    }

    /**
     * 当int 1需要 转换成01时调用
     */
    @JvmStatic
    fun formatInt00(targetInt: Int): String {
        val decimalFormat: Format = DecimalFormat("00")
        return decimalFormat.format(targetInt)
    }

    /**
     * 当int 1需要 转换成001时调用
     */
    fun formatInt000(targetInt: Int): String {
        val decimalFormat: Format = DecimalFormat("000")
        return decimalFormat.format(targetInt)
    }

    /**
     * 当int 1需要 转换成0001时调用
     */
    fun formatInt0000(targetInt: Int): String {
        val decimalFormat: Format = DecimalFormat("0000")
        return decimalFormat.format(targetInt)
    }

    /**
     * 当int 1需要 转换成00001时调用
     */
    fun formatInt00000(targetInt: Int): String {
        val decimalFormat: Format = DecimalFormat("00000")
        return decimalFormat.format(targetInt)
    }

    /**
     * float保留一位小数（4舍5入）
     */
    fun formatFloat1(targetFloat: Float): Float {
        val b = BigDecimal(targetFloat.toString())
        return b.setScale(1, BigDecimal.ROUND_HALF_UP).toFloat()
    }

}