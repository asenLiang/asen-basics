package com.asen.libcommon.base.viewbind

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.asen.libcommon.base.BaseActivity


/**
 * @author : asenLiang
 * @date   : 2020/12/3
 * @e-mail : liangAisiSen@163.com
 * @desc   : 基类封装 aac 模式
 */
abstract class BaseViewBindActivity : BaseActivity() {

    // TODO: ViewBinding绑定类
    abstract val mBinding: ViewBinding

    // TODO: viewModel绑定类
    open val mViewModel: BaseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT // 强制竖屏
        lifecycleScope.launchWhenStarted {
            // extImmersionBar(true)
            initView()
            initEvent()
        }
    }

    /** 初始类 */
    protected open suspend fun initView() {}
    protected open suspend fun initEvent() {}

}