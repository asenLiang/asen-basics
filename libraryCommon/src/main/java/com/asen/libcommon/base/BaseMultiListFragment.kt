package com.asen.libcommon.base

import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asen.libcommon.base.viewbind.BaseViewBindFragment
import com.asen.libcommon.base.viewbind.viewBind
import com.asen.libcommon.databinding.BaseListActivityBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshFooter
import com.scwang.smart.refresh.layout.api.RefreshHeader
import com.scwang.smart.refresh.layout.api.RefreshLayout
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @date   : 2020/12/17
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 多布局列表基类封装，可以有多个适配器的布局(使用谷歌推荐的ConcatAdapter适配器)
 */
abstract class BaseMultiListFragment(
    private var mAdapter: ConcatAdapter = ConcatAdapter()
) : BaseViewBindFragment() {

    override val mBinding: BaseListActivityBinding by viewBind()

    override suspend fun initView(view: View) {
        mBinding.apply {
            // 添加粘性头部
            if (getStickHeaderView() != null)
                headerContainer.addView(LayoutInflater.from(context).inflate(getStickHeaderView()!!, null))
            // 添加粘性尾部
            if (getStickFooterView() != null)
                footerContainer.addView(LayoutInflater.from(context).inflate(getStickFooterView()!!, null))

            // 添加粘性尾部
            mRecyclerView.adapter = mAdapter
            mRecyclerView.layoutManager = getLayoutManager()
            if (setItemDecoration() != null) mRecyclerView.addItemDecoration(setItemDecoration()!!)
        }

    }

    override suspend fun initEvent() {

        mBinding.apply {
            // 设置刷新列表头部动态视图
            if (onRefreshHeader() != null) mSmartRefreshLayout.setRefreshHeader(onRefreshHeader()!!)

            // 设置加载尾部动态视图
            if (onRefreshFooter() != null) mSmartRefreshLayout.setRefreshFooter(onRefreshFooter()!!)

            // 设置下拉刷新监听
            mSmartRefreshLayout.setEnableRefresh(openRefresh())
            mSmartRefreshLayout.setOnRefreshListener { onRefreshUpdate(it, true) }

            // 设置加载更多监听
            mSmartRefreshLayout.setEnableLoadMore(openLoadMore())
            mSmartRefreshLayout.setOnLoadMoreListener { onRefreshUpdate(it, false) }
        }

    }

    /** 空视/错误/网络异常 视图点击触发事件 */
    protected open fun onContainerClick(view: View) {}

    /** 设置列表RecyclerView.LayoutManager */
    protected open fun getLayoutManager(): RecyclerView.LayoutManager = LinearLayoutManager(context)

    /** 获取粘性头部布局（可以是标题栏或则其他视图） */
    @LayoutRes
    protected open fun getStickHeaderView(): Int? = null

    /** 获取粘性尾部布局（可以是按钮或tab） */
    @LayoutRes
    protected open fun getStickFooterView(): Int? = null

    /** 设置刷新列表头部动态视图 */
    protected open fun onRefreshHeader(): RefreshHeader? = null

    /** 设置加载尾部动态视图 */
    protected open fun onRefreshFooter(): RefreshFooter? = null

    /** 设置分割线 */
    protected open fun setItemDecoration(): DividerItemDecoration? = null

    /** 设置下拉刷新监听 */
    protected open fun openRefresh(): Boolean = true // 是否启用下拉刷新

    /** 设置加载更多监听 */
    protected open fun openLoadMore(): Boolean = true // 是否启用加载更多

    /** isRefresh:true刷新 false加载更多 */
    protected open fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {}

    /** 获取列表控件 */
    protected fun getRecyclerView() = mBinding.mRecyclerView

    /** 自动刷新 */
    protected fun autoRefresh(timeMillis: Long = 400) {
        mBinding.apply {

            if (openRefresh()) {
                mSmartRefreshLayout.autoRefresh(timeMillis.toInt())
            } else {
                lifecycleScope.launch {
                    delay(timeMillis)
                    onRefreshUpdate(mSmartRefreshLayout, true)
                    cancel()
                }
            }

        }
    }

    /** 自动加载更多 */
    protected fun autoLoadMore() {
        if (openLoadMore()) mBinding.mSmartRefreshLayout.autoLoadMore()
    }

    /** 设置列表数据 */
    protected fun <DATA> setList(adapter: BaseListAdapter<DATA>, data: MutableList<DATA>) {
        adapter.setList(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, data: MutableList<DATA>) {
        adapter.addData(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, data: DATA) {
        adapter.addData(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, position: Int, data: DATA) {
        adapter.addData(position, data)
    }

    /** 添加需要适配的适配器 */
    protected fun <DATA> addAdapter(
        adapter: BaseListAdapter<DATA>,
        isAnim: Boolean = true,                               // 是否开启动画
        animation: BaseQuickAdapter.AnimationType = BaseQuickAdapter.AnimationType.SlideInRight // 设置动画
    ): BaseListAdapter<DATA> {
        mAdapter.addAdapter(adapter)
        adapter.animationEnable = isAnim
        adapter.setAnimationWithDefault(animation)
        return adapter
    }

    /** 添加需要适配的适配器 */
    protected fun <DATA> addAdapter(
        @LayoutRes getItemLayoutId: Int,
        convert: ((vh: BaseViewHolder, t: DATA) -> Unit),
        isAnim: Boolean = true,
        animation: BaseQuickAdapter.AnimationType = BaseQuickAdapter.AnimationType.SlideInRight
    ) = addAdapter(
        BaseListAdapter(
            getItemLayoutId,
            convert
        ), isAnim, animation)

    /** 删除指定的适配器 */
    protected fun <DATA> removeAdapter(adapter: BaseListAdapter<DATA>) {
        mAdapter.removeAdapter(adapter)
    }

}