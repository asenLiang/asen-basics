package com.asen.libcommon.base

import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.asen.libcommon.base.viewbind.BaseViewBindActivity
import com.asen.libcommon.base.viewbind.viewBind
import com.asen.libcommon.databinding.BaseListActivityBinding
import com.chad.library.adapter.base.BaseQuickAdapter.AnimationType
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.layout.api.RefreshFooter
import com.scwang.smart.refresh.layout.api.RefreshHeader
import com.scwang.smart.refresh.layout.api.RefreshLayout
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/**
 * @author : asenLiang
 * @date   : 2020/12/3
 * @e-mail : liangAisiSen@163.com
 * @desc   : 多布局列表基类封装，可以有多个适配器的布局(使用谷歌推荐的ConcatAdapter适配器)
 */
abstract class BaseMultiListActivity(
    /** 谷歌推荐的ConcatAdapter适配器 */
    private var mAdapter: ConcatAdapter = ConcatAdapter()
) : BaseViewBindActivity() {

    override val mBinding: BaseListActivityBinding by viewBind()

    override suspend fun initView() {
        mBinding.apply {

            // todo：添加粘性尾部
            mRecyclerView.adapter = mAdapter
            mRecyclerView.layoutManager = getLayoutManager()
            if (setItemDecoration() != null) mRecyclerView.addItemDecoration(setItemDecoration()!!)
        }
    }

    override suspend fun initEvent() {
        mBinding.apply {
            // todo：设置刷新列表头部动态视图
            if (onRefreshHeader() != null) mSmartRefreshLayout.setRefreshHeader(onRefreshHeader()!!)

            // todo：设置加载尾部动态视图
            if (onRefreshFooter() != null) mSmartRefreshLayout.setRefreshFooter(onRefreshFooter()!!)

            // todo：设置下拉刷新监听
            mSmartRefreshLayout.setEnableRefresh(openRefresh())
            mSmartRefreshLayout.setOnRefreshListener { onRefreshUpdate(it, true) }

            // todo：设置加载更多监听
            mSmartRefreshLayout.setEnableLoadMore(openLoadMore())
            mSmartRefreshLayout.setOnLoadMoreListener { onRefreshUpdate(it, false) }
        }
    }



    /** 获取view） */
    protected  fun getView(@LayoutRes resId:Int): View {
        return LayoutInflater.from(mBinding.root.context).inflate(resId, null)
    }

    /** 空视/错误/网络异常 视图点击触发事件 */
    protected open fun onContainerClick(view: View) {}

    /** 设置列表RecyclerView.LayoutManager */
    protected open fun getLayoutManager(): RecyclerView.LayoutManager = LinearLayoutManager(this)

    /** 设置刷新列表头部动态视图 */
    protected open fun onRefreshHeader(): RefreshHeader? = null

    /** 设置加载尾部动态视图 */
    protected open fun onRefreshFooter(): RefreshFooter? = null

    /** 设置分割线 */
    protected open fun setItemDecoration(): DividerItemDecoration? = null

    /** 设置下拉刷新监听 */
    protected open fun openRefresh(): Boolean = true // 是否启用下拉刷新

    /** 设置加载更多监听 */
    protected open fun openLoadMore(): Boolean = true // 是否启用加载更多

    /** isRefresh:true刷新 false加载更多 */
    protected open fun onRefreshUpdate(it: RefreshLayout, isRefresh: Boolean) {}

    /** 自动刷新 */
    protected fun autoRefresh(timeMillis: Long = 400) {
        mBinding.apply {
            if (openRefresh()) {
                mSmartRefreshLayout.autoRefresh(timeMillis.toInt())
            } else {
                lifecycleScope.launch {
                    delay(timeMillis)
                    onRefreshUpdate(mSmartRefreshLayout, true)
                    cancel()
                }
            }
        }
    }

    /** 自动加载更多 */
    protected fun autoLoadMore() {
        if (openLoadMore()) mBinding.mSmartRefreshLayout.autoLoadMore()
    }

    /** 设置列表数据 */
    protected fun <DATA> setList(adapter: BaseListAdapter<DATA>, data: MutableList<DATA>) {
        if (data.size == 0) adapter.showEmptyView()
        adapter.setList(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, data: MutableList<DATA>) {
        adapter.addData(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, data: DATA) {
        adapter.addData(data)
    }

    /** 添加列表数据 */
    protected fun <DATA> addData(adapter: BaseListAdapter<DATA>, position: Int, data: DATA) {
        adapter.addData(position, data)
    }

    /** 添加需要适配的适配器 */
    protected fun <DATA> addAdapter(
        adapter: BaseListAdapter<DATA>,
        isAnim: Boolean = true,                               // 是否开启动画
        animation: AnimationType = AnimationType.SlideInRight // 设置动画
    ): BaseListAdapter<DATA> {
        mAdapter.addAdapter(adapter)
        adapter.animationEnable = isAnim
        adapter.setAnimationWithDefault(animation)
        return adapter
    }

    /** 添加需要适配的适配器 */
    protected fun <DATA> addAdapter(
        @LayoutRes getItemLayoutId: Int,
        convert: ((vh: BaseViewHolder, t: DATA) -> Unit),     // 回调用于布局控件设置
        isAnim: Boolean = true,
        animation: AnimationType = AnimationType.SlideInRight
    ) = addAdapter(BaseListAdapter(getItemLayoutId, convert), isAnim, animation)

    /** 删除指定的适配器 */
    protected fun <DATA> removeAdapter(adapter: BaseListAdapter<DATA>) {
        mAdapter.removeAdapter(adapter)
    }
}