package com.asen.libcommon.base.bind

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.asen.libcommon.eventBus.EventBusEntity
import com.asen.libcommon.eventBus.EventBusHelpLifecycleObserver
import com.asen.libcommon.eventBus.EventBusLifecycleObserver

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : base 基类封装 eventBus绑定类
 */

/**
 * 开启eventBus泛型回调
 * 接收 如：
 *        @Subscribe(threadMode = ThreadMode.MAIN)
 *        open fun onEvent(t: T) {
 *              callBack.invoke(event)
 *        }
 */
fun <T> FragmentActivity.extBindEventBusAny(callBack: (T) -> Unit) {
    lifecycle.addObserver(EventBusLifecycleObserver(callBack))
}

/**
 * 根据已经封装的 EventBusHelp 开放出来使用，在 activity 上面直接使用便可
 * 接收 如：
 *        bingEventBus { it ->
 *        }
 */
fun FragmentActivity.extBindEventBus(callBack: (EventBusEntity) -> Unit) {
    lifecycle.addObserver(EventBusHelpLifecycleObserver(callBack))
}


/**
 * 开启eventBus泛型回调
 * 接收 如：
 *        @Subscribe(threadMode = ThreadMode.MAIN)
 *        open fun onEvent(t: T) {
 *              callBack.invoke(event)
 *        }
 */
fun <T> Fragment.extBindEventBusAny(callBack: (T) -> Unit) {
    lifecycle.addObserver(EventBusLifecycleObserver(callBack))
}

/**
 * 根据已经封装的 EventBusHelp 开放出来使用，在 fragment 上面直接使用便可
 * 接收 如：
 *        bingEventBus { it ->
 *        }
 */
fun Fragment.extBindEventBus(callBack: (EventBusEntity) -> Unit) {
    lifecycle.addObserver(EventBusHelpLifecycleObserver(callBack))
}
