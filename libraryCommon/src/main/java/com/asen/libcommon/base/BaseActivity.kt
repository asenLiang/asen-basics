package com.asen.libcommon.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
//import cn.bingoogolapple.swipebacklayout.BGASwipeBackHelper
//import com.asen.libcommon.R
import io.reactivex.disposables.CompositeDisposable


/**
 *    @author : asenLiang
 *    @date   : 2020/12/3
 *    @desc   : 滑动返回基类封装,根據该基类拓展基类
 */
abstract class BaseActivity(
    /** rxJava 綁定 使用addDispose方法加入创建的rxJava,在销毁的时候统一处理防止内存泄漏*/
    internal var compositeDisposable: CompositeDisposable? = null
): AppCompatActivity()
    //, BGASwipeBackHelper.Delegate
{

    override fun onCreate(savedInstanceState: Bundle?) {
//        initSwipeBackFinish()
        super.onCreate(savedInstanceState)

        // TODO:注册eventBus 回调 直接在 activity 上面调用 bindEventBus便可回调接收消息
        //  if (isOpenEventBus) bindEventBus { onEventBus(it) }
    }

    /** EventBus 事件回调 */
    //protected open fun onEventBus(event: EventBusEntity) {}
    //protected open val isOpenEventBus = false // TODO:是否使用 eventBus

    //private var mSwipeBackHelper: BGASwipeBackHelper? = null

    /** 是否打开滑动返回上一个界面功能 */
//    open val openBGASwipeBackHelper = true

    /** 初始化滑动返回。在 super.onCreate(savedInstanceState) 之前调用该方法 */
//    private fun initSwipeBackFinish() {
//        if (openBGASwipeBackHelper) {
//            mSwipeBackHelper = BGASwipeBackHelper(this, this)
//
//            // TODO:「必须在 Application 的 onCreate 方法中执行 BGASwipeBackHelper.init 来初始化滑动返回」
//            // TODO:下面几项可以不配置，这里只是为了讲述接口用法。
//
//            // TODO:设置滑动返回是否可用。默认值为 true
//            mSwipeBackHelper?.setSwipeBackEnable(true)
//            // TODO:设置是否仅仅跟踪左侧边缘的滑动返回。默认值为 true
//            mSwipeBackHelper?.setIsOnlyTrackingLeftEdge(true)
//            // TODO:设置是否是微信滑动返回样式。默认值为 true
//            mSwipeBackHelper?.setIsWeChatStyle(true)
//            // TODO:设置阴影资源 id。默认值为 R.drawable.bga_sbl_shadow
//            mSwipeBackHelper?.setShadowResId(R.drawable.bga_sbl_shadow)
//            // TODO:设置是否显示滑动返回的阴影效果。默认值为 true
//            mSwipeBackHelper?.setIsNeedShowShadow(true)
//            // TODO:设置阴影区域的透明度是否根据滑动的距离渐变。默认值为 true
//            mSwipeBackHelper?.setIsShadowAlphaGradient(true)
//            // TODO:设置触发释放后自动滑动返回的阈值，默认值为 0.3f
//            mSwipeBackHelper?.setSwipeBackThreshold(0.3f)
//            // TODO:设置底部导航条是否悬浮在内容上，默认值为 false
//            mSwipeBackHelper?.setIsNavigationBarOverlap(false)
//        }
//    }

    /** 滑动返回执行完毕，销毁当前 Activity */
//    override fun onSwipeBackLayoutExecuted() {
//        if (openBGASwipeBackHelper) mSwipeBackHelper?.swipeBackward()
//    }

//    override fun onBackPressed() {
//        // TODO:正在滑动返回的时候取消返回按钮事件
//        if (openBGASwipeBackHelper) {
//            if (mSwipeBackHelper?.isSliding!!) return
//            mSwipeBackHelper?.backward()
//        }
//        super.onBackPressed()
//    }

    /** 正在滑动返回 slideOffset 从 0 到 1 */
//    override fun onSwipeBackLayoutSlide(slideOffset: Float) {}

    /** 没达到滑动返回的阈值，取消滑动返回动作，回到默认状态 */
//    override fun onSwipeBackLayoutCancel() {}

    /**
     * 是否支持滑动返回。这里在父类中默认返回 true 来支持滑动返回，
     * 如果某个界面不想支持滑动返回则重写该方法返回 false 即可
     */
//    override fun isSupportSwipeBack() = true
}