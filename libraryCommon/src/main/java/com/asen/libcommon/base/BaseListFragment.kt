package com.asen.libcommon.base

import android.view.View
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder


/**
 * @date   : 2020/12/3
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 普通列表基类封装，只有一个适配器的布局
 */
abstract class BaseListFragment<T>(
    private var mAdapter: BaseListAdapter<T>? = null
) : BaseMultiListFragment() {

    override suspend fun initView(view: View) {
        super.initView(view)
        mAdapter = addAdapter(getItemLayoutId(), this::convert)
        setAnimationWithDefault()
        // 适配器点击
        mAdapter?.setOnItemClickListener { a, v, p -> onItemClick(a as BaseListAdapter<T>, v, p) }
    }

    /** 设置Item布局 */
    @LayoutRes
    protected abstract fun getItemLayoutId(): Int

    /** Item布局回调 */
    protected abstract fun convert(vh: BaseViewHolder, t: T)

    /** 适配器点击 */
    protected open fun onItemClick(a: BaseListAdapter<T>, v: View, position: Int) {}

    /** 添加列表头部视图 */
    protected fun addHeaderView(view: View, index: Int = -1, o: Int = LinearLayout.VERTICAL) {
        mAdapter?.addHeaderView(view, index, o)
    }

    /** 添加列表尾部视图 */
    protected fun addFooterView(view: View, index: Int = -1, o: Int = LinearLayout.VERTICAL) {
        mAdapter?.addFooterView(view, index, o)
    }

    /** 设置列表数据 */
    protected fun setList(data: MutableList<T>) {
        setList(mAdapter!!, data)
    }

    /** 添加列表数据 */
    protected fun addData(data: MutableList<T>) {
        addData(mAdapter!!, data)
    }

    /** 设置动画 * @param ani 动画格式 * @param isEnable 是否开启动画 */
    protected fun setAnimationWithDefault(
        ani: BaseQuickAdapter.AnimationType = BaseQuickAdapter.AnimationType.SlideInRight,
        isEnable: Boolean = true
    ) {
        mAdapter?.animationEnable = isEnable
        mAdapter?.setAnimationWithDefault(ani)
    }

}