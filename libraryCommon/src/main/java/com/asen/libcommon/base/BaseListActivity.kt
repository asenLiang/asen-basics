package com.asen.libcommon.base

import android.view.View
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter.AnimationType
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * @author : asenLiang
 * @date   : 2020/12/3
 * @e-mail : liangAisiSen@163.com
 * @desc   : 普通列表基类封装，只有一个适配器的布局
 */
abstract class BaseListActivity<T> : BaseMultiListActivity() {

    private lateinit var mAdapter: BaseListAdapter<T>


    override suspend fun initView() {
        super.initView()
        mAdapter = addAdapter(getItemLayoutId(), this::convert)
        mBinding.mRecyclerView.adapter = mAdapter
        setAnimationWithDefault()
        // 适配器点击
        mAdapter.setOnItemClickListener { a, v, p -> onItemClick(a as BaseListAdapter<T>, v, p) }
    }

    /** 设置Item布局 */
    @LayoutRes
    protected abstract fun getItemLayoutId(): Int

    /** Item布局回调 */
    protected abstract fun convert(vh: BaseViewHolder, t: T)

    /** 适配器点击 */
    protected open fun onItemClick(a: BaseListAdapter<T>, v: View, position: Int) {}

    /** 添加列表头部视图 */
    protected fun addHeaderView(view: View, index: Int = -1, o: Int = LinearLayout.VERTICAL) {
        mAdapter.addHeaderView(view, index, o)
    }

    /** 添加列表尾部视图 */
    protected fun addFooterView(view: View, index: Int = -1, o: Int = LinearLayout.VERTICAL) {
        mAdapter.addFooterView(view, index, o)
    }

    /** 设置列表数据 */
    protected fun setList(data: MutableList<T>) {
        setList(mAdapter, data)
    }

    /** 添加列表数据 */
    protected fun addData(data: MutableList<T>) {
        addData(mAdapter, data)
    }

    /** 添加列表数据 */
    protected fun addData(data: T) {
        addData(mAdapter, data)
    }

    /** 添加列表数据 */
    protected fun addData(position:Int,data: T) {
        addData(mAdapter,position, data)
    }

    protected fun getData():MutableList<T>?{
        return mAdapter.data
    }

    protected fun getAdapter():BaseListAdapter<T>? = mAdapter

    /** 设置动画 * @param ani 动画格式 * @param isEnable 是否开启动画 */
    protected fun setAnimationWithDefault(ani: AnimationType = AnimationType.SlideInRight,isEnable:Boolean=true) {
        mAdapter.animationEnable = isEnable
        mAdapter.setAnimationWithDefault(ani)
    }


}