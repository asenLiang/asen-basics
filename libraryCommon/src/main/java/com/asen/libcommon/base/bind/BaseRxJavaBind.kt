package com.asen.libcommon.base.bind

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.asen.libcommon.base.viewbind.BaseViewBindFragment
import com.asen.libcommon.base.BaseActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : RxJava 任务管理器，每次创建rxJava处理任务的时候添加进来，
 *           在销毁的时候统一删除掉，防止内存泄漏
 */

/** 添加 RxJava 任务进来 */
fun BaseActivity.extAddDispose(disposable: Disposable?) {
    if (disposable!=null)lifecycle.addObserver(BaseRxJavaBind(compositeDisposable).addDispose(disposable))
}

/** 添加 RxJava 任务进来 */
fun BaseViewBindFragment.extAddDispose(disposable: Disposable?) {
    if (disposable!=null)lifecycle.addObserver(BaseRxJavaBind(compositeDisposable).addDispose(disposable))
}

/** 生命周期管理 */
class BaseRxJavaBind(private var compositeDisposable: CompositeDisposable? = null) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onCreate() {
        if (compositeDisposable == null) compositeDisposable = CompositeDisposable()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        // 保证 Activity 结束时取消所有正在执行的订阅
        if (compositeDisposable != null) compositeDisposable?.clear()
    }

    fun addDispose(disposable: Disposable): BaseRxJavaBind {
        disposable.let { compositeDisposable?.add(it) } // 将所有 Disposable 放入集中处理
        return this
    }

}