package com.asen.libcommon.base

import android.view.View
import com.asen.libcommon.R
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   : 列表适配器封装，直接在activity和fragment上面使用无需创建多个adapter
 */
class BaseListAdapter<T>(
    layoutResId: Int,
    private val convert: ((vh: BaseViewHolder, t: T) -> Unit)
) : BaseQuickAdapter<T, BaseViewHolder>(layoutResId, mutableListOf()) {

    override fun convert(vh: BaseViewHolder, t: T) {
        convert.invoke(vh, t)
    }

    fun showEmptyView(emptyView: Int = R.layout.view_multiple_status_empty) {
        removeEmptyView()
        val view = View.inflate(context, emptyView, null)
        data.clear()
        setEmptyView(view)
        view?.setOnClickListener {
            callEmptyClick?.invoke()
        }
        notifyDataSetChanged()
    }

    // todo：空视图回调
    private var callEmptyClick: (() -> Unit)? = null

    fun setCallEmptyClick(call: (() -> Unit)?) {
        callEmptyClick = call
    }

}