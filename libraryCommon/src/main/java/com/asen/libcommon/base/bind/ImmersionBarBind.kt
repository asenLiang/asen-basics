package com.asen.libcommon.base.bind

import android.app.Activity
import androidx.annotation.ColorRes
import androidx.annotation.FloatRange
import androidx.fragment.app.Fragment
import com.asen.libcommon.R
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ktx.immersionBar

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 状态栏工具
 */

/**
 * 设置沉浸式
 * @param isFitsSystemWindows Boolean 是否去重状态栏 布局与状态栏重叠问题 true去重 false重叠
 * @param isDarkFont Boolean 状态栏字体深色或亮色 isDarkFont isDarkFont – true 深色
 * @param isTransparentStatusBar Boolean 是否设置状态栏为透明背景
 * @param isTransparentNavigationBar Boolean 是否设置导航栏为透明背景
 * @param barColor Int 设置状态栏和导航栏颜色
 * @param statusAlpha Float 设置状态栏颜色透明度
 * @param navigationAlpha Float 设置导航栏颜色透明度
 * @param isKeyboardEnable Boolean 解决软键盘与底部输入框冲突问题 ，默认是false
 * @param barHide BarHide 设置隐藏导航栏和状态栏
 */
fun Activity.extImmersionBar(
    isFitsSystemWindows: Boolean = false,
    isDarkFont: Boolean = true,
    isTransparentStatusBar: Boolean = true,
    isTransparentNavigationBar: Boolean = false,
    @ColorRes barColor: Int = R.color.white_FFFFFF,
    @FloatRange(from = 0.0, to = 1.0) statusAlpha: Float = 0.04f,
    @FloatRange(from = 0.0, to = 1.0) navigationAlpha: Float = 0.08f,
    isKeyboardEnable: Boolean = false,
    barHide: BarHide = BarHide.FLAG_SHOW_BAR
) {
    immersionBar {
        /*
        * todo：解决状态栏和布局重叠问题，任选其一，默认为false，当为true时一定要指定statusBarColor()，
        *  不然状态栏为透明色，还有一些重载方法
        * (设置false不能设置statusBarColor，要不然重叠失效)
        */
        fitsSystemWindows(isFitsSystemWindows)

        // todo：图标是(true)深色/(false)亮色设置
        statusBarDarkFont(isDarkFont)
        navigationBarDarkIcon(isDarkFont)

        // todo：隐藏状态栏或导航栏或两者，不写默认不隐藏
        hideBar(barHide)

        // todo：透明状态栏，不写默认透明色
        if (isTransparentStatusBar){
            transparentStatusBar()
        }else{
            // todo：状态栏背景颜色设置
            statusBarColor(barColor)
            // todo：状态栏背景透明度设置
            statusBarAlpha(statusAlpha)
        }

        // todo：透明导航栏，不写默认黑色(设置此方法，fullScreen()方法自动为true)
        if (isTransparentNavigationBar) {
            transparentNavigationBar()
        }else{
            // todo：导航栏背景颜色设置
            navigationBarColor(barColor)
            // todo：导航栏背景透明度设置
            navigationBarAlpha(navigationAlpha)
        }

        // todo：解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
        keyboardEnable(isKeyboardEnable)

    }
}

/**
 * 设置沉浸式
 * @param isFitsSystemWindows Boolean 是否去重状态栏 布局与状态栏重叠问题 true去重 false重叠
 * @param isDarkFont Boolean 状态栏字体深色或亮色 isDarkFont isDarkFont – true 深色
 * @param isTransparentStatusBar Boolean 是否设置状态栏为透明背景
 * @param isTransparentNavigationBar Boolean 是否设置导航栏为透明背景
 * @param barColor Int 设置状态栏和导航栏颜色
 * @param statusAlpha Float 设置状态栏颜色透明度
 * @param navigationAlpha Float 设置导航栏颜色透明度
 * @param isKeyboardEnable Boolean 解决软键盘与底部输入框冲突问题 ，默认是false
 * @param barHide BarHide 设置隐藏导航栏和状态栏
 */
fun Fragment.extImmersionBar(
    isFitsSystemWindows: Boolean = false,
    isDarkFont: Boolean = true,
    isTransparentStatusBar: Boolean = true,
    isTransparentNavigationBar: Boolean = false,
    @ColorRes barColor: Int = R.color.white_FFFFFF,
    @FloatRange(from = 0.0, to = 1.0) statusAlpha: Float = 0.04f,
    @FloatRange(from = 0.0, to = 1.0) navigationAlpha: Float = 0.08f,
    isKeyboardEnable: Boolean = false,
    barHide: BarHide = BarHide.FLAG_SHOW_BAR
) {
    immersionBar {
        /*
        * todo：解决状态栏和布局重叠问题，任选其一，默认为false，当为true时一定要指定statusBarColor()，
        *  不然状态栏为透明色，还有一些重载方法
        * (设置false不能设置statusBarColor，要不然重叠失效)
        */
        fitsSystemWindows(isFitsSystemWindows)

        // todo：图标是(true)深色/(false)亮色设置
        statusBarDarkFont(isDarkFont)
        navigationBarDarkIcon(isDarkFont)

        // todo：隐藏状态栏或导航栏或两者，不写默认不隐藏
        hideBar(barHide)

        // todo：透明状态栏，不写默认透明色
        if (isTransparentStatusBar){
            transparentStatusBar()
        }else{
            // todo：状态栏背景颜色设置
            statusBarColor(barColor)
            // todo：状态栏背景透明度设置
            statusBarAlpha(statusAlpha)
        }

        // todo：透明导航栏，不写默认黑色(设置此方法，fullScreen()方法自动为true)
        if (isTransparentNavigationBar) {
            transparentNavigationBar()
        }else{
            // todo：导航栏背景颜色设置
            navigationBarColor(barColor)
            // todo：导航栏背景透明度设置
            navigationBarAlpha(navigationAlpha)
        }

        // todo：解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
        keyboardEnable(isKeyboardEnable)

    }
}