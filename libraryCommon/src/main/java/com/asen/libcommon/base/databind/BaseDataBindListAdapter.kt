package com.asen.libcommon.base.databind

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   : dataBind 基类封装 mvvm封装
 */
class BaseDataBindListAdapter<T>(
    layoutResId: Int,
    data: MutableList<T>?,
    private val convert: ((vh: BaseViewHolder, t: T) -> Unit)
) : BaseQuickAdapter<T, BaseDataBindListAdapter.BaseDataBindViewHolder>(layoutResId, data) {

    override fun convert(vh: BaseDataBindViewHolder, t: T) {
        // TODO:在 convert 回调加上三行代码就可双向绑定列表数据
        // val binding=vh.getDataBinding() as T
        // binding.itemRomm =t
        // binding.executePendingBindings()// TODO:防止列表闪烁
        convert.invoke(vh, t)
    }

    class BaseDataBindViewHolder(view: View) : BaseViewHolder(view) {

        private var binding = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }

        fun getDataBinding(): ViewDataBinding? {
            return binding
        }

    }



}

