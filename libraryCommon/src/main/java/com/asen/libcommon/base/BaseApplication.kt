package com.asen.libcommon.base

import android.app.Application
import android.content.Context
import com.asen.libcommon.config.BaseConfig
import com.luck.picture.lib.app.IApp
import com.luck.picture.lib.engine.PictureSelectorEngine
import com.orhanobut.logger.Logger


/**
 * @author : asenLiang
 * @date   : 2020/12/10
 * @e-mail : liangAisiSen@163.com
 * @desc   : app入口 基类封装
 * ----------Dragon be here!----------/
 * 　　　┏┓　　 ┏┓
 * 　　┏┛┻━━━┛┻┓━━━
 * 　　┃　　　　　 ┃
 * 　　┃　　　━　  ┃
 * 　　┃　┳┛　┗┳
 * 　　┃　　　　　 ┃
 * 　　┃　　　┻　  ┃
 * 　　┃　　　　   ┃
 * 　　┗━┓　　　┏━┛Code is far away from bug with the animal protecting
 * 　　　　┃　　　┃    神兽保佑,代码无bug
 * 　　　　┃　　　┃
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　　　　　┣┓
 * 　　　　┃　　　　　　　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛━━━━━
 * 　　　　　┃┫┫　┃┫┫
 * 　　　　　┗┻┛　┗┻┛
 * ━━━━━━━━━━━神兽出没━━━━━━━━━━━━━━
 */
class BaseApplication : Application(), IApp {

    companion object {
        private var mContext: Context? = null
        lateinit var application: Application
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
        application = this
        // 基本配置初始化
        BaseConfig.init(this)
        // 各模块初始化application配置,以后直接到模块下的ModuleInit配置环境就行,不需要再base基类配置，解耦分离
        onInit(this)
    }

    override fun getAppContext() = mContext

    override fun getPictureSelectorEngine(): PictureSelectorEngine? = null

    /** 反射遍历各模块的application 仿照测试模块 */
    private fun onInit(application: Application?) {
        for (moduleInitName
        in arrayOf(
            "com.asen.test.Application", // 测试模块     // 已添加
            "com.asen.app.Application",  // 基本模块     // 已添加
            "com.asen.main.Application", // 主业务模块   // 未添加
            "com.asen.logo.Application", // 登录验证模块 // 未添加
            "com.asen.home.Application", // 首页业务模块 // 未添加
            "com.asen.work.Application", // 工作业务模块 // 未添加
            "com.asen.msg.Application",  // 消息业务模块 // 未添加
            "com.asen.user.Application"  // 用户业务模块 // 未添加
        )) {
            try {
                val clazz = Class.forName(moduleInitName)
                val init: IBaseModule = clazz.newInstance() as IBaseModule
                Logger.e("初始化模块：$moduleInitName")
                // 调用初始化方法
                init.onInit(application)
            } catch (e: ClassNotFoundException) {
                Logger.e(e.message ?: "")
                e.printStackTrace()
            } catch (e: InstantiationException) {
                Logger.e(e.message ?: "")
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                Logger.e(e.message ?: "")
                e.printStackTrace()
            }
        }
    }


}