package com.asen.libcommon.base

import android.app.Application

/**
 * @date   : 2021/1/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 动态配置Application，有需要初始化的组件实现该接口，统一在主app的Application中初始化
 */
interface IBaseModule {
    //初始化优先的
    fun onInit(application: Application?): Boolean
}