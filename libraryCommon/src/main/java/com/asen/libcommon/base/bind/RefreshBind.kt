package com.asen.libcommon.base.bind

import com.scwang.smart.refresh.layout.api.RefreshLayout

/**
 * @date   : 2021/2/26
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : RefreshLayout 拓展函数
 */

//TODO: 结束加载 isRefresh 是否下拉刷新
fun RefreshLayout.extRefreshFinish(
    isRefresh: Boolean,
    isComplete: Boolean = false,
    success: Boolean = true, // 是否加载/刷新成功，默认成功
    call: (() -> Unit)? = null
) {
    when {
        isComplete -> {
            finishLoadMoreWithNoMoreData()// 关闭加载更多
        }
        isRefresh -> {
            finishRefresh(success)
        }
        else -> {
            finishLoadMore(success)
        }
    }
    call?.invoke()
}