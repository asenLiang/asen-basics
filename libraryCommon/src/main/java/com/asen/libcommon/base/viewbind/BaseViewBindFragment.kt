package com.asen.libcommon.base.viewbind

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.gyf.immersionbar.ktx.immersionBar
import io.reactivex.disposables.CompositeDisposable

/**
 * @date   : 2021/2/23
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : aac模式封装 -> 下次升级为 mvvm 模式
 */
abstract class BaseViewBindFragment(
    /** 视图是否加载完毕 */
    private var isViewPrepare: Boolean = false,
    /** 数据是否加载过了 */
    private var hasLoadData: Boolean = false,
    /** rxJava 綁定 使用addDispose方法加入创建的rxJava,在销毁的时候统一处理防止内存泄漏*/
    internal var compositeDisposable: CompositeDisposable? = null
) : Fragment() {

    // TODO: ViewBinding绑定类
    abstract val mBinding: ViewBinding
    // TODO: viewModel绑定类
    open val mViewModel: BaseViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launchWhenStarted {
            // TODO:注册eventBus 回调 直接在 fragment 上面调用 bindEventBus便可回调接收消息
            //  if (isOpenEventBus) bindEventBus { onEventBus(it) }
            initView(view)
            lazyLoadDataIfPrepared()
            initEvent()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            lazyLoadDataIfPrepared()
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) immersionBar()
    }

    /** 初始类 */
    protected open suspend fun initView(view: View) {}
    protected open suspend fun initEvent() {}

    /** EventBus 事件回调 */
    //protected open fun onEventBus(event: EventBusEntity) {}
    //protected open val isOpenEventBus = false // TODO:是否使用 eventBus

    /** 懒加载 */
    protected open fun lazyLoad() {}
    private fun lazyLoadDataIfPrepared() {
        if (userVisibleHint && isViewPrepare && !hasLoadData) {
            lazyLoad()
            hasLoadData = true
        }
    }

}