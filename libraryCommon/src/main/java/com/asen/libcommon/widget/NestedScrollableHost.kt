package com.asen.libcommon.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.ORIENTATION_HORIZONTAL
import kotlin.math.absoluteValue
import kotlin.math.sign

/**
 * @date   : 2021/4/16
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 解决 viewPager2 嵌套 viewPager2 同时是左右方向滑动的问题冲突
 *           使用方式： <xxx.NestedScrollableHost
                           android:layout_width="match_parent"
                           android:layout_height="match_parent">

                           <androidx.viewpager2.widget.ViewPager2
                           android:id="@+id/viewPager"
                           android:layout_width="match_parent"
                           android:layout_height="match_parent"/>

                        </xxx.NestedScrollableHost>
 */
class NestedScrollableHost : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    private var touchSlop = 0
    private var initialX = 0f
    private var initialY = 0f
    private val parentViewPager: ViewPager2?
        get() {
            var v: View? = parent as? View
            // 第一步：todo：遍历获取父容器等于 ViewPager2 就结束直 接跳转到第三步
            while (v != null && v !is ViewPager2) {
                // 第二部：todo：获取该 v 的父容器，如果父容器还是没有等 ViewPager2 就再遍历，因为 v的遍历条件没有结束
                v = v.parent as? View
            }
            // 第三步：todo：结束
            return v as? ViewPager2
        }

    private val child: View? get() = if (childCount > 0) getChildAt(0) else null

    init {
        touchSlop = ViewConfiguration.get(context).scaledTouchSlop
    }

    private fun canChildScroll(orientation: Int, delta: Float): Boolean {
        val direction = -delta.sign.toInt()
        return when (orientation) {
            0 -> child?.canScrollHorizontally(direction) ?: false
            1 -> child?.canScrollVertically(direction) ?: false
            else -> throw IllegalArgumentException()
        }
    }

    override fun onInterceptTouchEvent(e: MotionEvent): Boolean {
        handleInterceptTouchEvent(e)
        return super.onInterceptTouchEvent(e)
    }

    private fun handleInterceptTouchEvent(e: MotionEvent) {
        val orientation = parentViewPager?.orientation ?: return

        // 如果子对象不能和父对象朝同一方向滚动，请提前返回
        if (!canChildScroll(orientation, -1f) && !canChildScroll(orientation, 1f)) {
            return
        }

        when(e.action){
            MotionEvent.ACTION_DOWN->{
                initialX = e.x
                initialY = e.y
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_MOVE->{
                val dx = e.x - initialX
                val dy = e.y - initialY
                val isVpHorizontal = orientation == ORIENTATION_HORIZONTAL

                // assuming ViewPager2 touch-slop is 2x touch-slop of child
                val scaledDx = dx.absoluteValue * if (isVpHorizontal) .5f else 1f
                val scaledDy = dy.absoluteValue * if (isVpHorizontal) 1f else .5f

                if (scaledDx > touchSlop || scaledDy > touchSlop) {
                    if (isVpHorizontal == (scaledDy > scaledDx)) {
                        // Gesture is perpendicular, allow all parents to intercept
                        parent.requestDisallowInterceptTouchEvent(false)
                    } else {
                        // Gesture is parallel, query child if movement in that direction is possible
                        if (canChildScroll(orientation, if (isVpHorizontal) dx else dy)) {
                            // Child can scroll, disallow all parents to intercept  屏蔽父类的事件触摸
                            parent.requestDisallowInterceptTouchEvent(true)
                        } else {
                            // Child cannot scroll, allow all parents to intercept
                            parent.requestDisallowInterceptTouchEvent(false)
                        }
                    }
                }
            }
        }
    }
}