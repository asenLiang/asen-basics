package com.asen.libcommon.widget.recyclerViewTouchHelper

import android.graphics.Canvas
import android.graphics.Color
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.asen.libcommon.R
import kotlin.math.abs

/**
 * @date   : 2021/7/14
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 拖动/删除 控制列表封装帮助类
 */
class DragRemoveItemTouchHelperCallback(
    // todo：拖动背景颜色
    private val dragBg: Int = R.color.bg_gray,
    // todo：拖动结束背景颜色
    private val dragEndBg: Int = R.color.bg_gray
) : ItemTouchHelper.Callback() {


    /** item 删除回调 */
    var mItemRemove: ((position: Int) -> Unit)? = null

    /** item 位置替换回调 */
    var mItemMoveCallback: ((Int, Int) -> Boolean)? = null

    fun setCallback(itemMoveCallback: ((current: Int, target: Int) -> Boolean), itemRemove: ((position: Int) -> Unit)) {
        mItemMoveCallback = itemMoveCallback
        mItemRemove = itemRemove
    }

    override fun isLongPressDragEnabled() = true // 长按触发

    override fun getMovementFlags(rv: RecyclerView, vh: RecyclerView.ViewHolder): Int {

        // todo：需要实现上下拖拽
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        // todo：需要实现左右侧滑
        val swipeFlags = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(rv: RecyclerView, vh: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        // todo： 上下拖拽时，会一直回调该方法
        //   我们在该方法中实现item数据在数据集合中的位置交换，并调用Adapter的notifyItemMoved完成item界面刷新
        return mItemMoveCallback?.invoke(vh.adapterPosition, target.adapterPosition) ?: false
    }

    override fun onSwiped(vh: RecyclerView.ViewHolder, direction: Int) {
        mItemRemove?.invoke(vh.adapterPosition)
    }

    override fun onSelectedChanged(vh: RecyclerView.ViewHolder?, actionState: Int) {

        vh?.apply {
            // todo：根据被拖拽item的状态设置该item的显示样式
            if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                //拖拽时，设置阴影
                itemView.setBackgroundColor(itemView.context.resources.getColor(dragBg))
            } else {
                // todo：拖拽结束，去除阴影
                itemView.setBackgroundColor(itemView.context.resources.getColor(dragEndBg));
            }
        }
        super.onSelectedChanged(vh, actionState)
    }

    override fun clearView(rv: RecyclerView, vh: RecyclerView.ViewHolder) {

        // todo：恢复Item的原本效果
        vh.itemView.setBackgroundColor(Color.WHITE)
        vh.itemView.alpha = 1f //1-0
        super.clearView(rv, vh)
    }

    override fun onChildDraw(c: Canvas, rv: RecyclerView, vh: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        // todo：实现拖拽和滑动时的效果
        val alpha = 1 - abs(dX) / vh.itemView.width;
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            // todo：侧滑效果：透明度动画
            vh.itemView.alpha = alpha //1-0
        } else if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            // todo：拖拽效果：限制上下拖拽范围，只允许在2~10及之间拖拽
            if (vh.adapterPosition <= 2 || vh.adapterPosition >= 10) {
                return super.onChildDraw(c, rv, vh, dX, 0f, actionState, isCurrentlyActive);
            }
        }
        super.onChildDraw(c, rv, vh, dX, dY, actionState, isCurrentlyActive)
    }

    override fun canDropOver(rv: RecyclerView, current: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        // todo：设置拖拽可以释放的边界
        if (target.adapterPosition > 10) return false else if (target.adapterPosition < 2) return false
        return super.canDropOver(rv, current, target)
    }

}