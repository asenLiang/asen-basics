/*
 * Copyright (C) 2015-2017 Jacksgong(blog.dreamtobe.cn)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asen.libcommon.widget.keyboard.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

import com.asen.libcommon.R;
import com.asen.libcommon.widget.keyboard.iface.IPanelHeightTarget;

/**
 * @author : asenLiang
 * @date   : 2020/01/8
 * @e-mail : liangAisiSen@163.com
 * @desc   : 用于保存键盘高度，并提供有效的面板高度
 *           {@link #getValidPanelHeight(Context)}.
 *           <p/>
 *           调整面板高度与键盘高度刚好相关
 *           {@link #attach(Activity, IPanelHeightTarget)}.
 *
 *           @see KeyBoardSharedPreferences
 */
public class KeyboardUtil {

    public static void showKeyboard(final View view) {
        view.requestFocus();
        InputMethodManager inputManager =
                (InputMethodManager) view.getContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, 0);
    }

    public static void hideKeyboard(final View view) {
        InputMethodManager imm =
                (InputMethodManager) view.getContext()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private static int lastSaveKeyboardHeight = 0;

    private static boolean saveKeyboardHeight(final Context context, int keyboardHeight) {
        if (lastSaveKeyboardHeight == keyboardHeight) {
            return false;
        }

        if (keyboardHeight < 0) {
            return false;
        }

        lastSaveKeyboardHeight = keyboardHeight;
        Log.d("KeyBordUtil", String.format("save keyboard: %d", keyboardHeight));

        return KeyBoardSharedPreferences.save(context, keyboardHeight);
    }

    /**
     * @param context 键盘高度由shared-preferences存储，因此需要上下文context。
     * @return 存储的键盘高度
     * @see #getValidPanelHeight(Context)
     * @see #attach(Activity, IPanelHeightTarget)
     * <p/>
     * 处理并刷新键盘高度 {@link #attach(Activity, IPanelHeightTarget)}.
     */
    public static int getKeyboardHeight(final Context context) {
        if (lastSaveKeyboardHeight == 0) {
            lastSaveKeyboardHeight = KeyBoardSharedPreferences
                    .get(context, getMinPanelHeight(context.getResources()));
        }

        return lastSaveKeyboardHeight;
    }

    /**
     * @param context 键盘高度由shared-preferences存储，因此需要上下文context。
     * @return 有效的面板高度是指键盘高度
     * @see #getMaxPanelHeight(Resources)
     * @see #getMinPanelHeight(Resources)
     * @see #getKeyboardHeight(Context)
     * @see #attach(Activity, IPanelHeightTarget)
     * <p/>
     * 键盘高度可能太短或太高。我们截取键盘的高度
     * [{@link #getMinPanelHeight(Resources)}, {@link #getMaxPanelHeight(Resources)}].
     */
    public static int getValidPanelHeight(final Context context) {
        final int maxPanelHeight = getMaxPanelHeight(context.getResources());
        final int minPanelHeight = getMinPanelHeight(context.getResources());

        int validPanelHeight = getKeyboardHeight(context);

        validPanelHeight = Math.max(minPanelHeight, validPanelHeight);
        validPanelHeight = Math.min(maxPanelHeight, validPanelHeight);
        return validPanelHeight;
    }


    private static int maxPanelHeight = 0;
    private static int minPanelHeight = 0;
    private static int minKeyboardHeight = 0;

    public static int getMaxPanelHeight(final Resources res) {
        if (maxPanelHeight == 0) {
            maxPanelHeight = res.getDimensionPixelSize(R.dimen.max_panel_height);
        }

        return maxPanelHeight;
    }

    public static int getMinPanelHeight(final Resources res) {
        if (minPanelHeight == 0) {
            minPanelHeight = res.getDimensionPixelSize(R.dimen.min_panel_height);
        }

        return minPanelHeight;
    }

    public static int getMinKeyboardHeight(Context context) {
        if (minKeyboardHeight == 0) {
            minKeyboardHeight = context.getResources()
                    .getDimensionPixelSize(R.dimen.min_keyboard_height);
        }
        return minKeyboardHeight;
    }


    /**
     * 推荐由调用 {@link Activity#onCreate(Bundle)}
     * 例如，将键盘的高度尽可能与{@code target}对齐。
     * 对于保存，请将键盘高度刷新为共享首选项。
     *
     * @param activity ac
     * @param target   其高度将与键盘高度对齐。
     * @param lis      监听对象：键盘是否显示。
     * @see #saveKeyboardHeight(Context, int)
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static ViewTreeObserver.OnGlobalLayoutListener attach(final Activity activity,
                                                                 IPanelHeightTarget target,
                                                                 /* Nullable */
                                                                 OnKeyboardShowingListener lis) {
        final ViewGroup contentView = (ViewGroup) activity.findViewById(android.R.id.content);
        final boolean isFullScreen = ViewUtil.isFullScreen(activity);
        final boolean isTranslucentStatus = ViewUtil.isTranslucentStatus(activity);
        final boolean isFitSystemWindows = ViewUtil.isFitsSystemWindows(activity);

        // get the screen height.
        final Display display = activity.getWindowManager().getDefaultDisplay();
        final int screenHeight;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            final Point screenSize = new Point();
            display.getSize(screenSize);
            screenHeight = screenSize.y;
        } else {
            //noinspection deprecation
            screenHeight = display.getHeight();
        }

        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new KeyboardStatusListener(
                isFullScreen,
                isTranslucentStatus,
                isFitSystemWindows,
                contentView,
                target,
                lis,
                screenHeight);

        contentView.getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
        return globalLayoutListener;
    }

    /**
     * @see #attach(Activity, IPanelHeightTarget, OnKeyboardShowingListener)
     */
    public static ViewTreeObserver.OnGlobalLayoutListener attach(final Activity activity,
                                                                 IPanelHeightTarget target) {
        return attach(activity, target, null);
    }

    /**
     * 从活动根视图中删除OnGlobalLayoutListener
     *
     * @param activity 在相同的ac中使用 {@link #attach} 方法
     * @param l        ViewTreeObserver.OnGlobalLayoutListener returned by {@link #attach} method
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void detach(Activity activity, ViewTreeObserver.OnGlobalLayoutListener l) {
        ViewGroup contentView = (ViewGroup) activity.findViewById(android.R.id.content);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            contentView.getViewTreeObserver().removeOnGlobalLayoutListener(l);
        } else {
            //noinspection deprecation
            contentView.getViewTreeObserver().removeGlobalOnLayoutListener(l);
        }
    }

    private static class KeyboardStatusListener implements ViewTreeObserver.OnGlobalLayoutListener {
        private static final String TAG = "KeyboardStatusListener";

        private int previousDisplayHeight = 0;
        private final ViewGroup contentView;
        private final IPanelHeightTarget panelHeightTarget;
        private final boolean isFullScreen;
        private final boolean isTranslucentStatus;
        private final boolean isFitSystemWindows;
        private final int statusBarHeight;
        private boolean lastKeyboardShowing;
        private final OnKeyboardShowingListener keyboardShowingListener;
        private final int screenHeight;

        private boolean isOverlayLayoutDisplayHContainStatusBar = false;

        KeyboardStatusListener(boolean isFullScreen, boolean isTranslucentStatus,
                               boolean isFitSystemWindows,
                               ViewGroup contentView, IPanelHeightTarget panelHeightTarget,
                               OnKeyboardShowingListener listener, int screenHeight) {
            this.contentView = contentView;
            this.panelHeightTarget = panelHeightTarget;
            this.isFullScreen = isFullScreen;
            this.isTranslucentStatus = isTranslucentStatus;
            this.isFitSystemWindows = isFitSystemWindows;
            this.statusBarHeight = StatusBarHeightUtil.getStatusBarHeight(contentView.getContext());
            this.keyboardShowingListener = listener;
            this.screenHeight = screenHeight;
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
        @Override
        public void onGlobalLayout() {
            final View userRootView = contentView.getChildAt(0);
            final View actionBarOverlayLayout = (View) contentView.getParent();

            // 第一步。计算当前显示框的高度。
            Rect r = new Rect();

            final int displayHeight;
            final int notReadyDisplayHeight = -1;
            if (isTranslucentStatus) {
                // 状态栏半透明。

                // 在主题为状态栏半透明的情况下，我们假设displayHeight包含状态栏的高度，
                // 计算键盘状态（显示/隐藏）和键盘高度。

                actionBarOverlayLayout.getWindowVisibleDisplayFrame(r);

                final int overlayLayoutDisplayHeight = (r.bottom - r.top);

                if (!isOverlayLayoutDisplayHContainStatusBar) {
                    // 如果键盘隐藏，操作栏覆盖布局的显示高度可能等于屏幕高度。

                    // 如果 isOverlayLayoutDisplayHContainStatusBar 已为true，则操作栏覆盖布局的显示高度必须始终包含状态栏的高度。
                    isOverlayLayoutDisplayHContainStatusBar =
                            overlayLayoutDisplayHeight == screenHeight;
                }

                if (!isOverlayLayoutDisplayHContainStatusBar) {
                    // 在正常情况下，我们需要手动加上状态栏的高度
                    displayHeight = overlayLayoutDisplayHeight + statusBarHeight;
                } else {
                    // 在某些情况下（例如三星s7edge），动作栏的高度覆盖布局显示边界已经包含了状态栏的高度，
                    // 在这种情况下，我们不需要手动加上状态栏的高度
                    displayHeight = overlayLayoutDisplayHeight;
                }

            } else {
                if (userRootView != null) {
                    userRootView.getWindowVisibleDisplayFrame(r);
                    displayHeight = (r.bottom - r.top);
                } else {
                    Log.w("KeyBordUtil", "用户根视图没有准备好，所以忽略全局布局更改！");
                    displayHeight = notReadyDisplayHeight;
                }

            }

            if (displayHeight == notReadyDisplayHeight) {
                return;
            }

            calculateKeyboardHeight(displayHeight);
            calculateKeyboardShowing(displayHeight);

            previousDisplayHeight = displayHeight;
        }

        private void calculateKeyboardHeight(final int displayHeight) {
            // first result.
            if (previousDisplayHeight == 0) {
                previousDisplayHeight = displayHeight;

                // init the panel height for target.
                panelHeightTarget.refreshHeight(com.asen.libcommon.widget.keyboard.util.KeyboardUtil.getValidPanelHeight(getContext()));
                return;
            }

            int keyboardHeight;
            if (KPSwitchConflictUtil.isHandleByPlaceholder(isFullScreen, isTranslucentStatus,
                    isFitSystemWindows)) {
                // the height of content parent = contentView.height + actionBar.height
                final View actionBarOverlayLayout = (View) contentView.getParent();

                keyboardHeight = actionBarOverlayLayout.getHeight() - displayHeight;

                Log.d(TAG, String.format("action bar over layout %d display height: %d",
                        ((View) contentView.getParent()).getHeight(), displayHeight));

            } else {
                keyboardHeight = Math.abs(displayHeight - previousDisplayHeight);
            }
            // no change.
            if (keyboardHeight <= getMinKeyboardHeight(getContext())) {
                return;
            }

            Log.d(TAG, String.format("pre display height: %d display height: %d keyboard: %d ",
                    previousDisplayHeight, displayHeight, keyboardHeight));

            // 状态栏布局的影响。
            if (keyboardHeight == this.statusBarHeight) {
                Log.w(TAG, String.format("On global layout change get keyboard height just equal"
                        + " statusBar height %d", keyboardHeight));
                return;
            }

            // save the keyboardHeight
            boolean changed = com.asen.libcommon.widget.keyboard.util.KeyboardUtil.saveKeyboardHeight(getContext(), keyboardHeight);
            if (changed) {
                final int validPanelHeight = com.asen.libcommon.widget.keyboard.util.KeyboardUtil.getValidPanelHeight(getContext());
                if (this.panelHeightTarget.getHeight() != validPanelHeight) {
                    // 第三步。使用有效的面板高度刷新面板的高度，该高度是指最后一个键盘的高度
                    this.panelHeightTarget.refreshHeight(validPanelHeight);
                }
            }
        }

        private int maxOverlayLayoutHeight;

        private void calculateKeyboardShowing(final int displayHeight) {

            boolean isKeyboardShowing;

            // the height of content parent = contentView.height + actionBar.height
            final View actionBarOverlayLayout = (View) contentView.getParent();
            // 对于FragmentLayout，这不是真正的ActionBarOverlayLayout，它是LinearLayout，并且是
            // DecorView的子级，在这种情况下，它的顶部填充将等于状态栏的高度，其高度将等于
            // DecorViewHeight-NavigationBarHeight。
            final int actionBarOverlayLayoutHeight = actionBarOverlayLayout.getHeight()
                    - actionBarOverlayLayout.getPaddingTop();

            if (KPSwitchConflictUtil.isHandleByPlaceholder(isFullScreen, isTranslucentStatus,
                    isFitSystemWindows)) {
                if (!isTranslucentStatus
                        && actionBarOverlayLayoutHeight - displayHeight == this.statusBarHeight) {
                    // 处理状态栏布局，而不是键盘活动的情况
                    isKeyboardShowing = lastKeyboardShowing;
                } else {
                    isKeyboardShowing = actionBarOverlayLayoutHeight > displayHeight;
                }

            } else {

                final int phoneDisplayHeight = contentView.getResources()
                        .getDisplayMetrics().heightPixels;
                if (!isTranslucentStatus
                        && phoneDisplayHeight == actionBarOverlayLayoutHeight) {
                    // 没有空间可以放下状态栏，切换到全屏，只在暂停的情况下打开全屏页面。
                    Log.w(TAG, String.format("skip the keyboard status calculate, the current"
                                    + " activity is paused. and phone-display-height %d,"
                                    + " root-height+actionbar-height %d", phoneDisplayHeight,
                            actionBarOverlayLayoutHeight));
                    return;

                }

                if (maxOverlayLayoutHeight == 0) {
                    // non-used.
                    isKeyboardShowing = lastKeyboardShowing;
                } else {
                    isKeyboardShowing = displayHeight < maxOverlayLayoutHeight
                            - getMinKeyboardHeight(getContext());
                }

                maxOverlayLayoutHeight = Math
                        .max(maxOverlayLayoutHeight, actionBarOverlayLayoutHeight);
            }

            if (lastKeyboardShowing != isKeyboardShowing) {
                Log.d(TAG, String.format("displayHeight %d actionBarOverlayLayoutHeight %d "
                                + "keyboard status change: %B",
                        displayHeight, actionBarOverlayLayoutHeight, isKeyboardShowing));
                this.panelHeightTarget.onKeyboardShowing(isKeyboardShowing);
                if (keyboardShowingListener != null) {
                    keyboardShowingListener.onKeyboardShowing(isKeyboardShowing);
                }
            }

            lastKeyboardShowing = isKeyboardShowing;

        }

        private Context getContext() {
            return contentView.getContext();
        }
    }

    /**
     * 该接口用于监听键盘显示状态。
     *
     * @see #attach(Activity, IPanelHeightTarget, OnKeyboardShowingListener)
     * @see KeyboardStatusListener#calculateKeyboardShowing(int)
     */
    public interface OnKeyboardShowingListener {

        /**
         * 显示状态回调方法的键盘
         * <p>
         * 此方法在 {@link ViewTreeObserver.OnGlobalLayoutListener#onGlobalLayout()} 调用,
         * {@link ViewTreeObserver.OnGlobalLayoutListener#onGlobalLayout()} 是ViewTree生命周期回调方法之一。
         * 因此，建议不要使用那些耗时的操作（I/O、复杂计算、分配对象等），以免阻塞主ui线程。
         * </p>
         *
         * @param isShowing 指示是否显示键盘。
         */
        void onKeyboardShowing(boolean isShowing);

    }

}