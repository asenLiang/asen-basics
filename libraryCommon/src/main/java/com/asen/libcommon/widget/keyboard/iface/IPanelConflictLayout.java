/*
 * Copyright (C) 2015-2017 Jacksgong(blog.dreamtobe.cn)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asen.libcommon.widget.keyboard.iface;


/**
 * @author : asenLiang
 * @date   : 2020/01/8
 * @e-mail : liangAisiSen@163.com
 * @desc   : 用于面板容器布局的接口，用于非全屏主题窗口的情况。
 */
public interface IPanelConflictLayout {
    boolean isKeyboardShowing();

    /**
     * @return 可见与否的真实状态
     */
    boolean isVisible();

    /**
     * 键盘->面板
     *
     * @see com.asen.libcommon.widget.keyboard.util.KPSwitchConflictUtil#showPanel(android.view.View)
     */
    void handleShow();

    /**
     * 面板->键盘
     *
     * @see com.asen.libcommon.widget.keyboard.util.KPSwitchConflictUtil#showKeyboard
     */
    void handleHide();

    /**
     * @param isIgnoreRecommendHeight 忽略保证面板高度等于键盘高度。
     * @attr ref com.asen.libcommon.widget.keyboard.R.styleable#KPSwitchPanelLayout_ignore_recommend_height
     * @see com.asen.libcommon.widget.keyboard.handler.KPSwitchPanelLayoutHandler#resetToRecommendPanelHeight
     * @see com.asen.libcommon.widget.keyboard.util.KeyboardUtil#getValidPanelHeight(android.content.Context)
     */
    @SuppressWarnings("JavaDoc")
    void setIgnoreRecommendHeight(boolean isIgnoreRecommendHeight);
}
