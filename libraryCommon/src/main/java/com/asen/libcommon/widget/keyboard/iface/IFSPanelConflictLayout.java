/*
 * Copyright (C) 2015-2017 Jacksgong(blog.dreamtobe.cn)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asen.libcommon.widget.keyboard.iface;

import android.app.Activity;
import android.view.Window;


/**
 * @author : asenLiang
 * @date   : 2020/01/8
 * @e-mail : liangAisiSen@163.com
 * @desc   : 用于面板容器布局的接口，用于全屏主题窗口的情况。
 */
public interface IFSPanelConflictLayout {

    /**
     * 在{@link Activity#onPause()}方法记录当前键盘状态，并将在{@link Activity#onResume()}方法自动恢复键盘状态
     * <p/>
     * 建议在{@link Activity#onPause()}调用此方法，以记录右键盘状态和非布局冲突的键盘状态。
     * <p/>
     * 修复问题#12 Bug1和Bug2。
     *
     * @param window 当前视觉活动的当前窗口。
     */
    void recordKeyboardStatus(Window window);
}
