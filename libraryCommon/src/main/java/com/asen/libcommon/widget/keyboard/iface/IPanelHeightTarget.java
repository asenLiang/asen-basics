/*
 * Copyright (C) 2015-2017 Jacksgong(blog.dreamtobe.cn)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asen.libcommon.widget.keyboard.iface;

/**
 * @author : asenLiang
 * @date   : 2020/01/8
 * @e-mail : liangAisiSen@163.com
 * @desc   : 例如，将键盘的高度尽可能与面板高度对齐。
 *
 *           @see com.asen.libcommon.widget.keyboard.util.KeyboardUtil.KeyboardStatusListener
 */
public interface IPanelHeightTarget {

    /**
     * 对于手柄面板的高度，将等于上次保存的键盘高度。
     */
    void refreshHeight(int panelHeight);

    /**
     * @return 获取目标视图的高度。
     */
    int getHeight();

    /**
     * 由OnGloballYoutListener回调调用。
     *
     * @param showing 无论键盘是否显示。
     */
    void onKeyboardShowing(boolean showing);
}
