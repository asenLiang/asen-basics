/*
 * Copyright (C) 2015-2017 Jacksgong(blog.dreamtobe.cn)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asen.libcommon.widget.keyboard;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Window;
import android.widget.LinearLayout;

import com.asen.libcommon.widget.keyboard.handler.KPSwitchFSPanelLayoutHandler;
import com.asen.libcommon.widget.keyboard.iface.IFSPanelConflictLayout;
import com.asen.libcommon.widget.keyboard.iface.IPanelHeightTarget;
import com.asen.libcommon.widget.keyboard.util.KeyboardUtil;
import com.asen.libcommon.widget.keyboard.util.ViewUtil;

/**
 * @author : asenLiang
 * @date   : 2020/01/8
 * @e-mail : liangAisiSen@163.com
 * @desc   : 全屏幕主题窗口的面板容器线性布局，此布局的高度将始终等于键盘的高度。
 *           <p/>
 *           对于非全屏主题窗口，请使用 {@link com.asen.libcommon.widget.keyboard.KPSwitchPanelLinearLayout} instead.
 *
 *           @see KeyboardUtil#attach(android.app.Activity, IPanelHeightTarget)
 *           @see #recordKeyboardStatus(Window)
 */
public class KPSwitchFSPanelLinearLayout extends LinearLayout implements IPanelHeightTarget,
        IFSPanelConflictLayout {

    private KPSwitchFSPanelLayoutHandler panelHandler;

    public KPSwitchFSPanelLinearLayout(Context context) {
        super(context);
        init();
    }

    public KPSwitchFSPanelLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public KPSwitchFSPanelLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public KPSwitchFSPanelLinearLayout(Context context, AttributeSet attrs, int defStyleAttr,
                                       int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        panelHandler = new KPSwitchFSPanelLayoutHandler(this);
    }

    @Override
    public void refreshHeight(int panelHeight) {
        ViewUtil.refreshHeight(this, panelHeight);
    }

    @Override
    public void onKeyboardShowing(boolean showing) {
        panelHandler.onKeyboardShowing(showing);
    }


    @Override
    public void recordKeyboardStatus(final Window window) {
        panelHandler.recordKeyboardStatus(window);
    }
}
