package com.asen.libcommon.config

import android.app.Application
import android.graphics.Color
//import cn.bingoogolapple.swipebacklayout.BGASwipeBackHelper
import com.alibaba.android.arouter.launcher.ARouter
import com.asen.libcommon.BuildConfig
import com.asen.libcommon.exception.CrashHandler
import com.asen.libcommon.util.LogUtil
import com.asen.libcommon.util.SPUtil
import com.lxj.xpopup.XPopup
import com.ycbjie.webviewlib.X5WebUtils
import com.zchu.rxcache.RxCache
import com.zchu.rxcache.diskconverter.GsonDiskConverter


/**
 * @date   : 2020/12/4
 * @author : asenLiang
 * @e-mail : liangAisiSen@163.com
 * @desc   : 公用配置 - 静态封装
 */
object BaseConfig {

    val isDebug = BuildConfig.DEBUG // 项目是否是 debug 模式

    val sqlName = "asen" // 数据库名字

    fun init(application: Application) {
        // 侧滑
        //BGASwipeBackHelper.init(application, null)
        // 路由初始化
        initRouter(application)
        // 初始化日志
        LogUtil.initLogger()
        // X5WebUtils x5内核
        X5WebUtils.init(application)
        // 初始化缓存
        initRxCache()
        // 初始化弹窗
        initXPopup()
        // 初始化SP存储
        SPUtil.init(application)
        // 全程捕获异常信息
        CrashHandler().init(application, "asenLiang" + "/error")
    }

    /** 路由初始化 */
    private fun initRouter(application: Application) {
        if (isDebug) {
            ARouter.openLog()
            ARouter.openDebug()
        }
        ARouter.init(application)
    }

    /** 初始化缓存 */
    private fun initRxCache() {
        RxCache.initializeDefault(
            RxCache.Builder()
                .appVersion(1)
                // .diskDir(File(context?.cacheDir?.path + File.separator + "data-cache"))
                .diskConverter(GsonDiskConverter())
                .diskMax((20 * 1024 * 1024).toLong())
                .memoryMax(0)
                .setDebug(true)
                .build()
        )
    }

    /** 初始化弹窗 */
    private fun initXPopup() {
        XPopup.setPrimaryColor(Color.parseColor("#00B2FF"))
    }

}