# librarycommon

#### 介绍

公用模块

java:

     --> base   : 公用的Activity、fragment、adapter封装

     --> http   : 网络请求框架封装，包括公用的请求获取bean封装，拦截器

     --> util   : 公用工具类封装

     --> widget : 公用组件封装（不能包含项目特定组件，该封装的组件都是和项目没有关系的，可以在项目中直接使用）

res:

     --> drawable ：专门放自定义的 xml 图片 命名格式 xml_自定义

     --> layout   ：公用的布局 界面的列表、头部

     --> mipmap   : 放置只是与封装的公用控件有关的资源图片，所有项目的图片不能放在这里

Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)

